<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once APP_CTRL_PATH . '/record.ctrl.php';


/**
 * This controller manages actions that can be performed on custom sections
 *
 * @method Func_App App()
 */
class app_CtrlCustomSection extends app_CtrlRecord
{
    /**
     * @param string $itemId
     * @return Widget_VBoxLayout
     */
    public function availableDisplayFields($section = null, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $customSectionSet = $App->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $object = $customSection->object;
        $objectCtrl = $App->Controller()->$object(false);

        $allViewSections = $customSectionSet->select(
            $customSectionSet->view->is($customSection->view)->_AND_(
                $customSectionSet->object->is($customSection->object)
            )
        );

        $allViewFieldNames = array();
        foreach ($allViewSections as $viewSection) {
            $fields = $viewSection->getFields();
            foreach ($fields as $field) {
                $allViewFieldNames[$field['fieldname']] = $viewSection->view . ',' . $viewSection->rank . ',' . $viewSection->id;
            }
        }

        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->addItem(
            $W->Link(
                $App->translate('Create new field'),
                $App->Controller()->CustomField()->add($customSection->object)
            )->setOpenMode(widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC)
        );

        $availableFields = $objectCtrl->getAvailableDisplayFields();

        bab_Sort::asort($availableFields, 'description', bab_Sort::CASE_INSENSITIVE);

        foreach ($availableFields as $field) {
            $fieldName =  $field['name'];
            $fieldDescription = $field['description'];

            $used = isset($allViewFieldNames[$fieldName]);
            $box->addItem(
                $W->VBoxItems(
                    $W->Link(
                        $fieldDescription,
                        $this->proxy()->saveDisplayField($section, $fieldName)
                    )->addClass('widget-close-dialog', ($used ? 'widget-strong' : ''))
                    ->setTitle($fieldName)
                    ->setAjaxAction(),
                    $W->Hidden()->setName(array('sections', $field['name']))
                )->setSizePolicy('widget-list-element')
            );
        }

        $box->addClass('widget-3columns');

        $box->setReloadAction($this->proxy()->availableDisplayFields($section, $box->getId()));

        return $box;
    }


    /**
     *
     */
    public function addDisplayField($section = null, $filter = null)
    {
        $App = $this->App();

        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Available fields'));

        $box = $this->availableDisplayFields($section);

        $page->addItem($box);
        return $page;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return app_Page
     */
    public function editDisplayField($section, $fieldName)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $field = $customSection->getField($fieldName);

        if (!isset($field)) {
            return null;
        }

        $W = bab_Widgets();

        $object = $customSection->object;
        $objectCtrl = $App->Controller()->$object(false);

        $fieldName = $field['fieldname'];
        $params = $field['parameters'];

        $availableFields = $objectCtrl->getAvailableDisplayFields();

        $field = $availableFields[$fieldName];
        $fieldDescription = $field['description'];
        if (substr($fieldName, 0, 1) !== '_') {
            $fieldDescription = $App->translate($fieldDescription);
        }

        if (empty($field)) {
            //continue;
        }

        $page = $App->Ui()->Page();
        $page->setTitle($fieldDescription);

        $editor = new app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('section', $section);
        $editor->setHiddenValue('fieldName', $fieldName);

        $box = $W->VBoxItems(
            $W->LabelledWidget(
                $App->translate('Type'),
                $W->Select()
                    ->setName(array('parameters', 'type'))
                    ->addOption('', '')
                    ->addOption('title1', $App->translate('Title 1'))
                    ->addOption('title2', $App->translate('Title 2'))
                    ->addOption('title3', $App->translate('Title 3'))
            ),
            $W->LabelledWidget(
                $App->translate('Label'),
                $W->LineEdit()
                    ->setName(array('parameters', 'label'))
            ),
            $W->LabelledWidget(
                $App->translate('Class'),
                $W->LineEdit()
                    ->setName(array('parameters', 'classname'))
            ),
            $W->LabelledWidget(
                $App->translate('Size policy'),
                $W->LineEdit()
                    ->setName(array('parameters', 'sizePolicy'))
            )
        )->setVerticalSpacing(1, 'em');

        $editor->setValues($params, array('parameters'));

        $editor->addItem($box);

        $editor->setSaveAction($this->proxy()->saveDisplayField());

        $editor->isAjax = true;

        $page->addItem($editor);

        return $page;
    }



    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function saveDisplayField($section = null, $fieldName = null, $parameters = null)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));
        $customSection->removeField($fieldName);
        $customSection->addField($fieldName, $parameters);
        $customSection->save();

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }


    /**
     *
     * @param int $section
     * @param string $fieldName
     * @return boolean
     */
    public function removeDisplayField($section, $fieldName)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $customSection = $customSectionSet->request($customSectionSet->id->is($section));

        $customSection->removeField($fieldName);
        $customSection->save();
        $this->addReloadSelector('.depends-custom-sections');
        return true;
    }




    /**
     * @param string    $view
     * @param int       $id
     * @param string    $object
     * @return app_Page
     */
    public function edit($view, $id = null, $object = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $customSectionSet = $App->CustomSectionSet();
        if (isset($id)) {
            $record = $customSectionSet->request($id);
        } else {
            $record = $customSectionSet->newRecord();
            $record->object = $object;
        }

        $page = $App->Ui()->Page();

        $page->setTitle($App->translate('Section'));
        $page->addClass('app-page-editor');

        $editor = new app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('data[view]', $view);
        if ($record->id) {
            $editor->setHiddenValue('data[id]', $record->id);
        }
        $editor->setName('data');
        $editor->addItem(
            $W->Hidden()->setName('object')
        );

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Name'),
                $W->LineEdit()->setName('name')
            )
        );

        $sizePolicyClassnames = array(
            'col-md-1' => '1',
            'col-md-2' => '2',
            'col-md-3' => '3',
            'col-md-4' => '4',
            'col-md-5' => '5',
            'col-md-6' => '6',
            'col-md-7' => '7',
            'col-md-8' => '8',
            'col-md-9' => '9',
            'col-md-10' => '10',
            'col-md-11' => '11',
            'col-md-12' => '12',
            'left-panel' => 'left',
            'right-panel' => 'right',
        );


        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Size policy'),
                $W->Select()
                    ->addOptions($sizePolicyClassnames)
                    ->setName('sizePolicy')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Fields layout'),
                $W->Select()
                    ->addOptions(app_CustomSection::getFieldsLayouts())
                    ->setName('fieldsLayout')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Foldable'),
                $foldableCheckBox = $W->CheckBox()->setName('foldable')
            )
        );
        $editor->addItem(
            $foldedWidget = $W->LabelledWidget(
                $App->translate('Folded'),
                $W->CheckBox()->setName('folded')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Editable'),
                $W->CheckBox()->setName('editable')
            )
        );
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Class'),
                $W->LineEdit()->setName('classname')
            )
        );

        $foldableCheckBox->setAssociatedDisplayable($foldedWidget, array(true));

        $editor->setValues($record->getFormOutputValues(), array('data'));

        $editor->addButton(
            $W->SubmitButton(/*'save'*/)
                ->validate(true)
                ->setAction($this->proxy()->save())
                ->setAjaxAction()
            ->setLabel($App->translate('Save'))
        );

        $editor->addButton(
            $W->SubmitButton(/*'cancel'*/)
                ->addClass('widget-close-dialog')
            ->setLabel($App->translate('Cancel'))
        );

        $page->addItem($editor);

        return $page;
    }


    /**
     *
     * @param int $id
     * @return app_Page
     */
    public function editVisibility($id = null)
    {
        $App = $this->App();

        $page = $App->Ui()->Page();

        $page->setTitle($App->translate('Section visibility'));
        $page->addClass('app-page-editor');

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $object = $record->object . 'Set';

        $objectRecordSet = $App->$object();


        $editor = $App->Ui()->CriteriaEditor($objectRecordSet->all());
        $editor->setHiddenValue('data[id]', $record->id);
        $editor->setName('data');

        if (!empty($record->visibilityCriteria)) {
            $arrCriteria = json_decode($record->visibilityCriteria, true);

            $data = array();
            foreach ($arrCriteria as $fieldName => $operation) {
                $data['field'] = $fieldName;
                foreach ($operation as $condition => $value) {
                    $data['condition']['default'] = $condition;
                    $data['condition']['bool'] = $condition;
                    $data['value'] = $value;
                }
            }

            $editor->setValues($data, array('data'));
        }


        $editor->setSaveAction($this->proxy()->saveVisibility());
        $editor->isAjax = bab_isAjaxRequest();

        $page->addItem($editor);

        return $page;
    }



    /**
     * {@inheritDoc}
     * @see app_Controller::save()
     */
    public function save($data = null)
    {
        parent::save($data);

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }


    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::delete()
     */
    public function delete($id)
    {
        parent::delete($id);

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }


    /**
     * @param array $data
     */
    public function saveVisibility($data = null)
    {
        $App = $this->App();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($data['id']);

        $object = $record->object . 'Set';

        $objectRecordSet = $App->$object();

        $field = $data['field'];

        $field = $objectRecordSet->$field;

        $condition = $data['condition'];

        if ($field instanceof ORM_BoolInterface) {
            $condition = $condition['bool'];
        } else {
            $condition = $condition['default'];
        }

        $criteria = $field->{$condition}($data['value']);

        $record->visibilityCriteria = $criteria->toJson();

        $record->save();

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }






    /**
     *
     * @param string $view
     * @param string $itemId
     * @return Widget_Form
     */
    public function sections($object, $view, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $objectCtrl = $App->Controller()->$object(false);

        $availableFields = $objectCtrl->getAvailableDisplayFields();

        $sectionsSection = $W->VBoxItems();

        $customSectionCtrl = $App->Controller()->CustomSection();

        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->all(
                $customSectionSet->object->is($object),
                $customSectionSet->view->is($view)
            )
        );
        $customSections->orderAsc($customSectionSet->rank);

        $sectionsBoxes = array();
        /* @var $rows Widget_Layout[] */
        $rows = array();

        $currentColumn = 0;
        $row = $W->FlowItems()->addClass('connectedSortable'); //->setSizePolicy('row');
        $row->addClass('widget-sameheight-elements');
        $rows[] = $row;

        $nbCol = 0;
        foreach ($customSections as $customSection) {

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            $currentColumn += $nbCol;

            $sectionSection = $W->Section(
                $customSection->name . ' (' . $customSection->rank . ')',
                $sectionBox = $W->VBoxItems($W->Hidden()->setName('#' . $customSection->id))
            );
            $sectionSection->addClass('app-custom-section');
            $sectionSection->addClass($customSection->classname);
            $sectionSection->setSizePolicy($customSection->sizePolicy . ' widget-sameheight');

            $row->addItem($sectionSection);

            $menu = $sectionSection->addContextMenu('inline');
            $menu->addItem(
                $W->HBoxItems(
                    $W->Link('', $customSectionCtrl->addDisplayField($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->edit($view, $customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->editVisibility($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG),
                    $W->Link('', $customSectionCtrl->confirmDelete($customSection->id))
                        ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                )
            );
            $fields = $customSection->getFields();
            foreach ($fields as $fieldId => $field) {
                if (empty($field)) {
                    continue;
                }

                $fieldName = $field['fieldname'];

                $field = $availableFields[$fieldName];
                $fieldDescription = $field['description'];
                if (substr($fieldName, 0, 1) !== '_') {
                    $fieldDescription = $App->translate($fieldDescription);
                }

                if (empty($field)) {
                    continue;
                }

                $fieldItem = $W->HBoxItems(
                    $W->Label($fieldDescription)->setSizePolicy('maximum')->addClass('icon', Func_Icons::ACTIONS_ZOOM_FIT_HEIGHT),
                    $W->HBoxItems(
                        $W->Hidden()->setName($customSection->id . '.' . $fieldId)->setValue($field['name']),
                        $W->Link('', $customSectionCtrl->editDisplayField($customSection->id, $field['name']))
                            ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                            ->setOpenMode(Widget_Link::OPEN_DIALOG),
                        $W->Link('', $customSectionCtrl->removeDisplayField($customSection->id, $field['name']))
                            ->addClass('icon', Func_Icons::ACTIONS_LIST_REMOVE)
                            ->setAjaxAction()
                    )->addClass('widget-actions')
                )->setSizePolicy('widget-list-element widget-actions-target')
                ->addClass(Func_Icons::ICON_LEFT_16);

                $sectionBox->addItem($fieldItem);
            }

            $sectionsBoxes[] = $sectionBox;
        }

        if ($currentColumn + $nbCol > 0) {
            $sectionsSection->addItem($row);
        }

        foreach ($sectionsBoxes as $sectionsBox) {
            $sectionsBox->sortable(true, $sectionsBoxes);
            $sectionsBox->setMetadata('samePlaceholderSize', true);
        }

        foreach ($rows as $row) {
            $row->sortable(true, $rows);
            $row->setMetadata('samePlaceholderSize', true);
        }

        $form = $W->Form();

        if (isset($itemId)) {
            $form->setId($itemId);
        }
        $form->setHiddenValue('tg', $App->controllerTg);
        $form->setName('sections');
        $form->addItem($sectionsSection);
        $form->setAjaxAction($this->proxy()->saveSections(), $sectionsSection, 'sortstop');
        $form->setReloadAction($this->proxy()->sections($object, $view, $form->getId()));

        $form->addClass('depends-custom-sections');

        return $form;
    }



    /**
     *
     * @param string $view
     */
    public function exportSections($object, $view)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $customSections = $customSectionSet->select(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($object))
        );

        $sectionsValues = array();
        foreach ($customSections as $customSection) {
            $values = $customSection->getValues();
            unset($values['id']);
            unset($values['createdBy']);
            unset($values['createdOn']);
            unset($values['modifiedBy']);
            unset($values['modifiedOn']);
            unset($values['deletedBy']);
            unset($values['deletedOn']);
            unset($values['deleted']);
            $sectionsValues[] = $values;
        }

        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename="' . $object . '.' . ($view === '' ? 'default' : $view) . '.json"'."\n");

        $json = bab_json_encode($sectionsValues);
        $json = bab_convertStringFromDatabase($json, bab_charset::UTF_8);

        die($json);
    }




    /**
     * @param string $view
     * @return app_Page
     */
    public function importSectionsConfirm($object, $view = '')
    {
        $App = $this->App();
        $W = bab_Widgets();

        $page = $App->Ui()->Page();

        $page->setTitle($App->translate('Import sections layout'));

        $editor = new app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('object', $object);

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('View layout name'),
                $W->LineEdit(),
                'view'
            )
        );

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('File'),
                $W->FilePicker(),
                'sectionsfile'
            )
        );

        $editor->setSaveAction($this->proxy()->importSections());

        $page->addItem($editor);

        return $page;
    }


    /**
     * @param string $view
     * @param string $sectionsfile
     * @return bool
     */
    public function importSections($object = null, $view = null, $sectionsfile = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $files = $W->FilePicker()->getFolder();
        $files->push('sectionsfile');
        $files->push($sectionsfile['uid']);

        $data = null;
        foreach ($files as $f) {
            $data = file_get_contents($f->toString());
            break;
        }

        if (!isset($data)) {
            $this->addError($App->translate('Unable to get file content.'));
            return true;
        }

        $sectionsValues = json_decode($data, true);

        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $customSectionSet->delete(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($object))
        );

        foreach ($sectionsValues as $sectionValues) {
            $customSection = $customSectionSet->newRecord();
            $customSection->setValues($sectionValues);
            $customSection->object = $object;
            $customSection->view = $view;
            $customSection->save();
        }

        return true;
    }



    /**
     *
     * @param string $view
     * @return bool
     */
    public function deleteSections($object, $view)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $objectName = $this->getRecordClassName();

        $customSectionSet->delete(
            $customSectionSet->view->is($view)->_AND_($customSectionSet->object->is($objectName))
            );

        return true;
    }


    /**
     * Display an user interface to edit sections associated to a specific object.
     *
     * @param string $object
     * @param string $view
     * @return app_Page
     */
    public function editSections($object, $view)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $Ui = $App->Ui();

        $page = $Ui->Page();

        $page->setTitle(sprintf($App->translate('Edit view layout %s'), $view));

        $toolbar = $Ui->Toolbar();

        $toolbar->addItem(
            $W->Link(
                $App->translate('Delete view layout'),
                $this->proxy()->deleteSections($object, $view)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
        );

        $toolbar->addItem(
            $W->Link(
                $App->translate('Export view layout'),
                $this->proxy()->exportSections($object, $view)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
        );

        $toolbar->addItem(
            $W->Link(
                $App->translate('Add a section'),
                $this->proxy()->edit($view, null, $object)
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG)
        );

        $form = $this->sections($object, $view);

        $page->addItem($toolbar);
        $page->addItem($form);

        return $page;
    }



    public function saveSections($sections = null)
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();

        $sectionsFields = array();
        $sectionsRawFields = array();

        $rank = 0;
        foreach (array_keys($sections) as $sectionFieldId) {

            if (substr($sectionFieldId, 0, 1) === '#') {
                $currentSectionId = substr($sectionFieldId, 1);

                $customSection = $customSectionSet->request($customSectionSet->id->is($currentSectionId));

                $sectionsFields[$currentSectionId] = array();
                $sectionsRawFields[$currentSectionId] = $customSection->getRawFields();

                $customSection->rank = $rank;
                $rank++;
                $customSection->save();
                continue;
            }

            $sectionsFields[$currentSectionId][] = $sectionFieldId;
        }

        foreach ($sectionsFields as $sectionId => $sectionFieldIds) {
            foreach ($sectionFieldIds as $sectionFieldId) {
                list($srcSectionId, $srcfieldId) = explode('.', $sectionFieldId);
                if (!isset($sectionsRawFields[$srcSectionId][$srcfieldId])) {
                    return true;
                }
            }
        }

        foreach ($sectionsFields as $sectionId => $sectionFieldIds) {
            $newRawFields = array();
            foreach ($sectionFieldIds as $sectionFieldId) {
                list($srcSectionId, $srcfieldId) = explode('.', $sectionFieldId);
                $newRawFields[] = $sectionsRawFields[$srcSectionId][$srcfieldId];
            }

            $customSection = $customSectionSet->request($customSectionSet->id->is($sectionId));
            $customSection->fields = implode(',', $newRawFields);
            $customSection->save();
        }

        $this->addReloadSelector('.depends-custom-sections');

        return true;
    }
}
