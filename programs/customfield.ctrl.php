<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once APP_CTRL_PATH . '/record.ctrl.php';

/**
 * This controller manages actions that can be performed on custom fields
 */
class app_CtrlCustomField extends app_CtrlRecord
{

    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $toolbar = new app_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $App->translate('Add a custom field'),
                $this->proxy()->edit()
            )->addClass('icon',  Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        return $toolbar;
    }


    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::display()
     */
    public function display($id)
    {

    }


    /**
     * Display form to add a custom field on specified object class.
     *
     * @param string $object     'Contact', 'Organization'...
     * @throws app_AccessException
     * @return app_Page
     */
    public function add($object = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Add a custom field'));

        $set = $App->CustomFieldSet();
        $customfield = $set->newRecord();
        $customfield->object = $object;

        $form = $Ui->CustomFieldEditor($customfield);

        $page->addItem($form);

        return $page;
    }



	/**
	 * @return Widget_Page
	 */
	public function edit($customfield = null)
	{
		$W = bab_Widgets();
		$App = $this->App();
		$Ui = $App->Ui();
		$page = $Ui->Page();

		$page->addClass('app-page-editor');
		$page->setTitle($App->translate('Edit custom field'));

		if (null !== $customfield) {
		    $set = $App->CustomFieldSet();
			$customfield = $set->request($customfield);
		}

		$form = $Ui->CustomFieldEditor($customfield);

		$page->addItem($form);

		if ($customfield instanceof app_CustomField) {
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);

			$actionsFrame->addItem($W->Link($W->Icon($App->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($customfield->id)));
		}

		return $page;
	}


	/**
	 * @return bool
	 */
	public function save($customfield = null)
	{
	    $App = $this->App();

	    $set = $App->CustomFieldSet();

		if (empty($customfield['id'])) {
			$record = $set->newRecord();
		} else {
			$record = $set->get($customfield['id']);
		}

		if ('Enum' === $customfield['fieldtype'] || 'Set' === $customfield['fieldtype']) {
		    $enumvalues = array();
		    foreach ($customfield['enumvalues'] as $enumkey => $enumvalue) {
		        $enumvalues[$enumkey] = $set->enumvalues->input($enumvalue);
		    }
			$record->enumvalues = serialize($enumvalues);
		} else {
			$record->enumvalues = '';
		}

		unset($customfield['enumvalues']);
		$record->setFormInputValues($customfield);

		$record->save();

		// refresh target table structure

		$object = $record->object.'Set';
		$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);

		$recordSet = $App->$object();
		if (method_exists($recordSet, 'useLang')) {
		    // This is necessary if the recordSet constructor uses a setLang().
		    // We need to revert to multilang fields before synchronizing.
		    $recordSet->useLang(false);
		}

		$sql = $mysqlbackend->setToSql($recordSet);

		require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		$synchronize = new bab_synchronizeSql();
		$synchronize->fromSqlString($sql);

		return true;
	}


    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::delete()
     */
	public function delete($customfield)
	{
	    $App = $this->App();

	    if (!$customfield) {
		    throw new app_AccessException($App->translate('Access denied'));
		}

		$set = $App->CustomFieldSet();
		$record = $set->request($customfield);
		$object = $record->object.'Set';
		$set->delete($set->id->is($customfield));

		$recordSet = $App->$object();
		if (method_exists($recordSet, 'useLang')) {
		    // This is necessary if the recordSet constructor uses a setLang().
		    // We need to revert to multilang fields before synchronizing.
		    $recordSet->useLang(false);
		}

		$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
		$sql = $mysqlbackend->setToSql($recordSet);

		require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		$synchronize = new bab_synchronizeSql();
		$synchronize->fromSqlString($sql);

		return true;
	}
}
