<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 *
 * @copyright Copyright (c) 2008-2018 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2019 by CapWelton ({@link https://www.capwelton.com})
 */

/**
 *
 * @property ORM_StringField    $name
 * @property ORM_TextField      $description
 *
 * @method app_Role     get(mixed $criteria)
 * @method app_Role     request(mixed $criteria)
 * @method app_Role[]   select(\ORM_Criteria $criteria = null)
 * @method app_Role     newRecord()
 */
class app_RoleSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App)
    {
        parent::__construct($App);

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_TextField('description')
                ->setDescription('Description')
        );
    }
}

/**
 *
 * @property string $name
 * @property string $description
 *
 * @method app_RoleSet getParentSet()
 */
class app_Role extends app_Record
{
}
