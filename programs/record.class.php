<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


$App = app_App();
$App->includeBase();


/**
 *
 * @property ORM_PkField        $id
 *
 * @method app_Record   get(mixed $criteria)
 * @method app_Record   request(mixed $criteria)
 * @method app_Record[] select(\ORM_Criteria $criteria = null)
 * @method app_Record   newRecord()
 */
class app_RecordSet extends ORM_RecordSet implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    protected $app = null;

    protected $accessRights = null;

    /**
     * @var app_AccessManager
     */
    private $accessManager = null;

    /**
     * @var array
     */
    private $customFields = null;


    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app)
    {
        parent::__construct();
        $this->setApp($app);
        $this->accessRights = array();

        $this->setAccessManager($app->AccessManager());
        $this->setPrimaryKey('id');
    }

    /**
     * {@inheritDoc}
     * @see app_Object_Interface::setApp()
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see app_Object_Interface::App()
     */
    public function App()
    {
        return $this->app;
    }

    /**
     * @param string $setName
     */
    protected function extractSetPrefixAndName($setName)
    {
        $pos = strrpos($setName, '\\');
        if ($pos === false) {
            return explode('_', $setName);
        }
        return array(substr($setName, 0, $pos), substr($setName, $pos + 1));
    }

    /**
     * Similar to the ORM_RecordSet::join() method but instanciates the
     * joined RecordSet using App methods.
     *
     * @see ORM_RecordSet::join()
     */
    public function join($fkFieldName)
    {
        $fkField = $this->getField($fkFieldName);
        if (!($fkField instanceof ORM_FkField)) {
            return $this;
        }
        $setName = $fkField->getForeignSetName();

        if (!$setName || 'Set' === $setName) {
            throw new Exception('The set name is missing on foreign key field '.$fkFieldName);
        }

        list($prefix, $appSetName) = $this->extractSetPrefixAndName($setName);
        $set = $this->App()->$appSetName();
        $set->setName($fkField->getName());
        $set->setDescription($fkField->getDescription());

        $this->aField[$fkFieldName] = $set;
        $set->setParentSet($this);
        return $this;
    }


//     /**
//      *
//      * @param string $accessName
//      * @param string $type
//      */
//     public function addAccessRight($accessName, $type = 'acl')
//     {
//         $this->accessRights[$accessName] = $type;
//     }


//     /**
//      * @return array
//      */
//     public function getAccessRights()
//     {
//         return $this->accessRights;
//     }



    /**
     * @return app_CustomField[]
     */
    public function getCustomFields()
    {
        $App = $this->App();

        if (null === $this->customFields) {
            $this->customFields = array();

            if (isset($App->CustomField)) {
                $customFieldSet = $App->CustomFieldSet();
                $object = mb_substr(get_class($this), mb_strlen($App->classPrefix), -mb_strlen('Set'));
                try {
                    $customFields = $customFieldSet->select($customFieldSet->object->is($object));

                    foreach ($customFields as $customfield) {
                        $this->customFields[] = $customfield;

                        /*@var $customfield app_CustomField */

                    }
                } catch (ORM_BackEndSelectException $e) {
                    // table does not exist, this error is thrown by the install program while creating the sets
                }
            }
        }

        return $this->customFields;
    }




    /**
     * @return app_CustomField[]
     */
    public function selectCustomFields()
    {
        $App = $this->App();

        $customFieldSet= $App->CustomFieldSet();
        $object = mb_substr(get_class($this), mb_strlen($App->classPrefix), -mb_strlen('Set'));

        $customFields = $customFieldSet->select($customFieldSet->object->is($object));

        return $customFields;
    }


    /**
     * @return self
     */
    public function addCustomFields()
    {
        $customFields = $this->selectCustomFields();
        foreach ($customFields as $customField) {
            /*@var $customField app_CustomField */
            $description = $customField->name;
            $ormField = $customField->getORMField()
                ->setDescription($description);
            if ($ormField instanceof ORM_FkField) {
                $this->hasOne($customField->fieldname, $ormField->getForeignSetName())
                    ->setDescription($description);
            } else {
                $this->addFields($ormField);
            }
        }

        return $this;
    }


    /**
     * Similar to ORM_RecordSet::get() method  but throws a app_NotFoundException if the
     * record is not found.
     *
     * @since 1.0.18
     *
     * @throws ORM_Exception
     * @throws app_NotFoundException
     *
     * @param ORM_Criteria|string $mixedParam    Criteria for selecting records
     *                                           or the value for selecting record
     * @param string              $sPropertyName The name of the property on which
     *                                           the value applies. If not
     *                                           specified or null, the set's
     *                                           primary key will be used.
     *
     * @return app_Record
     */
    public function request($mixedParam = null, $sPropertyName = null)
    {
        $record = $this->get($mixedParam, $sPropertyName);
        if (!isset($record)) {
            // This will remove the default criteria for TraceableRecords and
            // fetch even 'deleted' ones.
            $this->setDefaultCriteria(null);
            $record = $this->get($mixedParam, $sPropertyName);
            if (isset($record)) {
                throw new app_DeletedRecordException($record, $mixedParam);
            }
            throw new app_NotFoundException($this, $mixedParam);
        }
        return $record;
    }









    /**
     * Returns an iterator of traceableRecord linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @param	app_Record | array		$source			source can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        if (is_array($source) || ($source instanceof Iterator)) {
            return $linkSet->selectForSources($source, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForSource($source, $this->getRecordClassName(), $linkType);
        }
    }

    /**
     * Returns an iterator of traceableRecord linked to the specified target,
     * optionally filtered on the specified link type.
     *
     * @param	app_Record | array		$target			target can be an array of record
     * @param	string					$linkType
     *
     * @return ORM_Iterator
     */
    public function selectLinkedFrom($target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        if (is_array($target) || ($target instanceof Iterator)) {
            return $linkSet->selectForTargets($target, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForTarget($target, $this->getRecordClassName(), $linkType);
        }
    }


    /**
     * Returns a criteria usable to select records of this set which are source of app_Links to the specified app_Record.
     *
     * @param app_Record $target
     * @param string     $linkType
     * @return ORM_Criteria
     */
    public function isSourceOf(app_Record $target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $linkSet->hasOne('sourceId', get_class($this));

        $criteria =	$linkSet->targetClass->is(get_class($target))
            ->_AND_($linkSet->targetId->is($target->id))
            ->_AND_($linkSet->sourceClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }



    /**
     * Returns a criteria usable to select records of this set which are target of app_Links to the specified app_Record.
     *
     * @param app_Record            $source
     * @param null|string|string[]  $linkType
     * @return ORM_Criteria
     */
    public function isTargetOf(app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $linkSet->hasOne('targetId', get_class($this));

        $criteria =	$linkSet->sourceClass->is(get_class($source))
            ->_AND_($linkSet->sourceId->is($source->id))
            ->_AND_($linkSet->targetClass->is($this->getRecordClassName()));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are target of app_Links from the specified app_Records.
     *
     * @since 1.0.23
     *
     * @param app_Record[]          $sources
     * @param null|string|string[]  $linkType
     * @return ORM_Criteria
     */
    public function isTargetOfAny($sources, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        $linkSet->hasOne('targetId', get_class($this));

        $sourceIdsByClasses = array();
        foreach ($sources as $source) {
            $sourceClass = get_class($source);
            if (!isset($sourceIdsByClasses[$sourceClass])) {
                $sourceIdsByClasses[$sourceClass] = array();
            }
            $sourceIdsByClasses[$sourceClass][] = $source->id;
        }

        $sourcesCriteria = array();
        foreach ($sourceIdsByClasses as $sourceClass => $sourceIds) {
            $sourcesCriteria[] = $linkSet->sourceClass->is($sourceClass)->_AND_($linkSet->sourceId->in($sourceIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->targetClass->is($this->getRecordClassName()),
            $linkSet->any($sourcesCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * Returns a criteria usable to select records of this set which are source of app_Links to the specified app_Records.
     *
     * @since 1.0.23
     *
     * @param app_Record[]     $targets
     * @param string|null      $linkType
     * @return ORM_Criteria
     */
    public function isSourceOfAny($targets, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();
        $linkSet->hasOne('sourceId', get_class($this));

        $targetIdsByClasses = array();
        foreach ($targets as $target) {
            $targetClass = get_class($target);
            if (!isset($targetIdsByClasses[$targetClass])) {
                $targetIdsByClasses[$targetClass] = array();
            }
            $targetIdsByClasses[$targetClass][] = $target->id;
        }

        $targetsCriteria = array();
        foreach ($targetIdsByClasses as $targetClass => $targetIds) {
            $targetsCriteria[] = $linkSet->targetClass->is($targetClass)->_AND_($linkSet->targetId->in($targetIds));
        }

        $criteria =	$linkSet->all(
            $linkSet->sourceClass->is($this->getRecordClassName()),
            $linkSet->any($targetsCriteria)
        );
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $criteria = $this->id->in($criteria);

        return $criteria;
    }


    /**
     * @param app_AccessManager $accessManager
     * @return self
     */
    public function setAccessManager(app_AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;
        return $this;
    }


    /**
     * @return app_AccessManager
     */
    public function getAccessManager()
    {
        return $this->accessManager;
    }


    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return false;
    }



    /**
     *
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->hasAccess('read');
    }

    /**
     *
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->hasAccess('update');
    }

    /**
     *
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->hasAccess('delete');
    }



    /**
     * Returns a criterion matching records that can be put to trash by the current user.
     *
     * @return ORM_Criterion
     */
    public function isRemovable()
    {
        return $this->isUpdatable();
    }


    /**
     * Returns a criterion matching records that can be restored from the trash by the current user.
     *
     * @return ORM_Criterion
     */
    public function isRestorable()
    {
        return $this->isUpdatable();
    }

    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @param string    $accessType
     * @param int|null  $user
     *
     * @return ORM_Criterion
     */
    public function hasAccess($accessType, $user = null)
    {
        $accessManager = $this->getAccessManager();
        return $accessManager->getAccessCriterion($this, $accessType, $user);
    }
    
    /**
     * Returns a criteria usable to select records of this set associated to the specified tags.
     *
     * @array	$tags		An array of tag ids
     * @string	$type		The link type [optional]
     * @return ORM_Criteria
     */
    public function haveTagLabels($tagLabels, $linkType = null)
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();

        /* @var $tagSet app_RecordSet */
        $tagSet = $App->TagSet();
        $tagClassName = $tagSet->getRecordClassName();
        
        $linkSet->joinTarget($tagClassName);
        $criteria = $linkSet->sourceClass->is($this->getRecordClassName())
            ->_AND_($linkSet->targetClass->is($tagClassName));
        
        if (is_array($tagLabels)) {
            $criteria = $criteria->_AND_($linkSet->targetId->label->in($tagLabels));
        } else {
            $criteria = $criteria->_AND_($linkSet->targetId->label->is($tagLabels));
        }
        if (isset($linkType)) {
            $criteria = $criteria->_AND_($linkSet->type->is($linkType));
        }
        $links = $linkSet->select($criteria);
        
        $ids = array();
        foreach ($links as $link) {
            $ids[$link->sourceId] = $link->sourceId;
        }
        
        return $this->id->in($ids);
    }
}



/**
 * @property int        $id
 *
 * @method app_RecordSet getParentSet()
 */
class app_Record extends ORM_Record implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    protected $app = null;




    /**
     * Returns the value of the specified field or an iterator if $sFieldName represents a 'Many relation'.
     *
     * @param string $sFieldName The name of the field or relation for which the value must be returned.
     * @param array  $args       Optional arguments.
     *
     * @return mixed The value of the field or null if the field is not a part of the record.
     */
    public function __call($sFieldName, $args)
    {
        $value = $this->oParentSet->getBackend()->getRecordValue($this, $sFieldName);
        $field = $this->oParentSet->$sFieldName;
        if (!is_null($value) && $field instanceof ORM_FkField) {

            $sClassName = $field->getForeignSetName();

            $App = $this->App();
            $prefixLength = mb_strlen($App->classPrefix);
            $methodName = mb_substr($sClassName, $prefixLength);
            $set = $App->$methodName();

            $set->setName($field->getName());
            $set->setDescription($field->getDescription());

            $record = $set->get($value);
            return $record;
        }
        return $value;
    }

    /**
     * {@inheritDoc}
     * @see app_Object_Interface::setApp()
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Get APP object to use with this record
     *
     * @return Func_App
     */
    public function App()
    {
        if (!isset($this->app)) {
            // If the app object was not specified (through the setApp() method),
            // we set it as parent set's App.
            $this->setApp($this->getParentSet()->App());
        }
        return $this->app;
    }


    /**
     * Returns the base class name of a record.
     * For example xxx_Contact will return 'Contact'.
     *
     * @since 1.0.40
     * @return string
     */
    public function getClassName()
    {
        $App = $this->App();
        $rClass = new ReflectionClass(get_class($this));
        $component = $App->getComponentByName($rClass->getShortName());
        if(isset($component)){
            return $component->getRecordClassName();
//             list(, $classname) = explode('_', $component->getRecordClassName());
//             return $classname;
        }
        list(, $classname) = explode('_', get_class($this));
        return $classname;
    }


    /**
     * Returns the string reference corresponding to the record.
     *
     * @return string 	A reference string (e.g. Contact:12)
     */
    public function getRef()
    {
        if (!isset($this->id)) {
            throw new app_Exception('Trying to get the reference string of a record without an id.');
        }
        $classname = $this->getClassName();
        return $classname . ':' . $this->id;
    }


    /**
     * @return app_Controller
     */
    public function getController()
    {
        $App = $this->App();

        $ctrlName = $this->getClassName();
        return $App->Controller()->$ctrlName();
    }


    /**
     * Deletes the record with respect to referential integrity.
     *
     * Uses referential integrity as defined by hasManyRelation to delete/update
     * referenced elements.
     *
     * @see app_RecordSet::hasMany()
     *
     * @return self
     */
    public function delete($deletedStatus = null)
    {
        $App = $this->App();

        if (!isset($deletedStatus)) {
            $deletedStatus = app_TraceableRecord::DELETED_STATUS_DELETED;
        }

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Uses referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();

            switch ($manyRelation->getOnDeleteMethod()) {

                case ORM_ManyRelation::ON_DELETE_SET_NULL:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->$foreignSetFieldName = 0;
                        $foreignRecord->save();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_CASCADE:

                    $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

                    foreach ($foreignRecords as $foreignRecord) {
                        $foreignRecord->delete();
                    }

                    break;

                case ORM_ManyRelation::ON_DELETE_NO_ACTION:
                default:
                    break;

            }
        }


        // We remove all links to and from this record.
        $linkSet = $App->LinkSet();

        $linkSet->delete(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
            ->_OR_(
                $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
            )
        );


        $set->delete($set->$recordIdName->is($recordId), $deletedStatus);

        return $this;
    }



    /**
     * Reassociates all data asociated to the record to another
     * specified one.
     *
     * @param	int		$id
     *
     * @return self
     */
    public function replaceWith($id)
    {
        $App = $this->App();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();


        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();
            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();
            // $foreignSet = new $foreignSetClassName($App);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));

            foreach ($foreignRecords as $foreignRecord) {
                $foreignRecord->$foreignSetFieldName = $id;
                $foreignRecord->save();
            }
        }


        // We replace all links to and from this record.
        $linkSet = $App->LinkSet();

        $links = $linkSet->select(
            $linkSet->sourceClass->is(get_class($this))->_AND_($linkSet->sourceId->is($recordId))
        );

        foreach ($links as $link) {
            $link->sourceId = $id;
            $link->save();
        }

        $links = $linkSet->select(
            $linkSet->targetClass->is(get_class($this))->_AND_($linkSet->targetId->is($recordId))
        );

        foreach ($links as $link) {
            $link->targetId = $id;
            $link->save();
        }

        return $this;
    }


    /**
     *
     *
     * @return array
     */
    public function getRelatedRecords()
    {
        $App = $this->App();

        $set = $this->getParentSet();
        $recordIdName = $set->getPrimaryKey();
        $recordId = $this->$recordIdName;

        // Use referential integrity as defined by hasManyRelation to delete/update
        // referenced elements.
        $manyRelations = $set->getHasManyRelations();

        $relatedRecords = array();

        foreach ($manyRelations as $manyRelation) {
            /* @var $manyRelation ORM_ManyRelation */

            $foreignSetClassName = $manyRelation->getForeignSetClassName();
            $foreignSetFieldName = $manyRelation->getForeignFieldName();

            $method = mb_substr($foreignSetClassName, mb_strlen($App->classPrefix));
            $foreignSet = $App->$method();
            // $foreignSet = new $foreignSetClassName($App);
            $foreignRecords = $foreignSet->select($foreignSet->$foreignSetFieldName->is($recordId));


            if ($foreignRecords->count() > 0) {
                $relatedRecords[$foreignSetClassName] = $foreignRecords;
            }
        }

        return $relatedRecords;
    }






    /**
     * Upload path for record attachments
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        $path = $this->App()->uploadPath();

        if (null === $path)
        {
            throw new Exception('Missing upload path information');
            return null;
        }

        $path->push(get_class($this));
        $path->push($this->id);

        return $path;
    }






    /**
     * @return bool
     */
    public function isLinkedTo(app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $criteria =	$linkSet->sourceClass->is(get_class($source))
        ->_AND_($linkSet->sourceId->is($source->id))
        ->_AND_($linkSet->targetClass->is(get_class($this)))
        ->_AND_($linkSet->targetId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * @return bool
     */
    public function isSourceOf(app_Record $target, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $criteria =	$linkSet->all(
            $linkSet->targetClass->is(get_class($target)),
            $linkSet->targetId->is($target->id),
            $linkSet->sourceClass->is(get_class($this)),
            $linkSet->sourceId->is($this->id)
        );

        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);

        $isLinked = ($links->count() > 0);

        $linkSet->__destruct();
        unset($linkSet);

        return $isLinked;
    }



    /**
     * Link record to $source
     *
     * @param app_Record $source
     * @param string $linkType
     * @param string $data
     * @return self
     */
    public function linkTo(app_Record $source, $linkType = '', $data = '')
    {
        $linkSet = $this->App()->LinkSet();

        $link = $linkSet->create($source, $this, $linkType, $data);

        $link->__destruct();
        unset($link);

        $linkSet->__destruct();
        unset($linkSet);

        return $this;
    }

    /**
     * Unlink record from $source
     *
     * @return self
     */
    public function unlinkFrom(app_Record $source, $linkType = null)
    {
        $linkSet = $this->App()->LinkSet();

        $linkSet->deleteLink($source, $this, $linkType);

        return $this;
    }


    /**
     * import a value into a traceable record property if the value is not equal
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importProperty($name, $value)
    {
        if (((string) $this->$name) !== ((string) $value)) {
            $this->$name = $value;
            return 1;
        }

        return 0;
    }



    /**
     * import a value into a tracable record property if the value is not equal, try with multiple date format
     * this method work for date field 0000-00-00
     *
     * @param	string	$name		property name
     * @param	mixed	$value		value to set
     *
     * @return int		1 : the value has been modified | 0 : no change
     */
    protected function importDate($name, $value)
    {
        if (preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/',$value)) {
            return $this->importProperty($name, $value);
        }

        // try in DD/MM/YYYY format

        if (preg_match('/(?P<day>[0-9]+)\/(?P<month>[0-9]+)\/(?P<year>[0-9]{2,4})/',$value, $matches)) {

            $value = sprintf('%04d-%02d-%02d', (int) $matches['year'], (int) $matches['month'], (int) $matches['day']);

            return $this->importProperty($name, $value);
        }

    }




    /**
     *
     * @return string[]
     */
    public function getViews()
    {
        $App = $this->App();

        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select($customSectionSet->object->is($this->getClassName()));
        $customSections->groupBy($customSectionSet->view);

        $views = array();
        foreach ($customSections as $customSection) {
            $views[] = $customSection->view;
        }

        if (empty($views)) {
            $views[] = '';
        }

        return $views;
    }



    /**
     * Checks if the record is readable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isReadable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record is updatable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isUpdatable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isUpdatable()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Checks if the record is deletable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isDeletable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isDeletable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record can be put to the trash by the current user.
     * @return bool
     */
    public function isRemovable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isRemovable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record can be restored from the trash by the current user.
     * @return bool
     */
    public function isRestorable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isRestorable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Ensures that the record is readable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireReadable($message = null)
    {
        if (!$this->isReadable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }

    /**
     * Ensures that the record is updatable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireUpdatable($message = null)
    {
        if (!$this->isUpdatable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }

    /**
     * Ensures that the record is deletable by the current user or throws an exception.
     * @since 1.0.40
     * @param string $message
     * @throws app_AccessException
     */
    public function requireDeletable($message = null)
    {
        if (!$this->isDeletable()) {
            $App = $this->App();
            if (!isset($message)) {
                $message = $App->translate('Access denied');
            }
            throw new app_AccessException($message);
        }
    }
}

