<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

define('APP_PHP_PATH', dirname(__FILE__) . '/');
define('APP_SET_PATH', dirname(__FILE__) . '/');
define('APP_UI_PATH', dirname(__FILE__) . '/ui/');
define('APP_WIDGETS_PATH', APP_UI_PATH . 'widgets/');
define('APP_CTRL_PATH', dirname(__FILE__) . '/');

require_once APP_PHP_PATH . 'base.class.php';
require_once APP_UI_PATH . 'base.ui.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';



class app_Exception extends Exception
{
}


/**
 * app_NotFoundException are thrown when the user tries to
 * access a non-existent app object using the request() method.
 *
 * @see app_RecordSet::request()
 *
 * @since 1.0.18
 */
class app_NotFoundException extends app_Exception
{
    private $recordSet = null;

    private $id = null;

    /**
     * @param ORM_RecordSet $recordSet
     * @param string $id
     */
    public function __construct(ORM_RecordSet $recordSet, $id)
    {
        $this->recordSet = $recordSet;
        $this->id = $id;
    }

    /**
     * @return ORM_RecordSet
     */
    public function getRecordSet()
    {
        return $this->recordSet;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the set description and translate
     * @return string
     */
    public function getObjectTitle()
    {
        $element = app_translate('object');
        if ($description = $this->getRecordSet()->getDescription()) {
            $element = mb_strtolower(app_translate($description));
        }

        return $element;
    }
}


/**
 * app_DeletedRecordException are thrown when the user tries to
 * access a deleted app object using the request() method.
 *
 * @see app_RecordSet::request()
 *
 * @since 1.0.21
 */
class app_DeletedRecordException extends app_NotFoundException
{
    /**
     * @var ORM_Record
     */
    private $record = null;

    /**
     * @param ORM_Record $record
     */
    public function __construct(ORM_Record $record)
    {
        parent::__construct($record->getParentSet(), $record->id);
        $this->record = $record;
    }

    /**
     * @return string
     */
    public function getDeletedBy()
    {
        if (!$this->record->deletedBy) {
            return null;
        }

        $W = bab_Widgets();
        return $W->Label(sprintf(app_translate('Deleted by: %s'), bab_getUserName($this->record->deletedBy)));
    }


    /**
     * @return string
     */
    public function getDeletedOn()
    {
        $set = $this->getRecordSet();
        if (!$set->deletedOn->isValueSet($this->record->deletedOn)) {
            return null;
        }

        $W = bab_Widgets();
        return $W->Label(sprintf(app_translate('Deleted on: %s'), bab_shortDate(bab_mktime($this->record->deletedOn))));
    }

}


/**
 * app_AccessException are thrown when the user tries to
 * perform an action or access a page that she is not
 * allowed to.
 */
class app_AccessException extends app_Exception
{
    /**
     * Require credential if the user is not logged in
     * @var bool
     */
    public $requireCredential = true;
}

/**
 * app_EmptyResultException are thrown for empty search results
 *
 */
class app_EmptyResultException extends app_Exception
{
    public $page;

    public function __construct($message, Widget_Page $page)
    {
        $this->page = $page;
    }
}



/**
 * app_SaveException are thrown in save methods of controller.
 * this will go back to previous page in breadcrumb history (the edit form)
 *
 */
class app_SaveException extends app_Exception
{
    /**
     * Relative position in breadcrumbs history
     * the save action is in position 0 (do not use position 0)
     * @var int
     */
    public $breadcrumbs_position = -1;

    /**
     * true : redirect to failed action
     * false : execute failed action directly without redirect
     * 		(can be used to redirect ot the same form and populate
     * 		 fields with posted data)
     *
     * @var bool
     */
    public $redirect = true;
}
