; <?php/*

[general]
name							="LibApp"
version							="0.1.0"
encoding						="UTF-8"
description						="Application framework"
description.fr					="Framework applicatif"
delete							=1
db_prefix						="app_"
ov_version						="8.6.97"
php_version						="5.4.0"
mysql_version					="5.1"
addon_access_control			=0
author							="Laurent Choulette(laurent.choulette@cantico.fr)"
icon							="icon.png"
mysql_character_set_database	="latin1,utf8"

[addons]
LibOrm							="0.11.14"
widgets							="1.0.90"
LibFileManagement				="0.2.23"

;*/?>