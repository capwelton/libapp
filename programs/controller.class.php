<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . 'utilit/controller.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/json.php';

require_once dirname(__FILE__). '/functions.php';

$App = app_App();
if ($App) {
    $App->includeBase();
}


class app_UnknownActionException extends Exception
{
	public function __construct($action, $code = 0)
	{
		$message = 'Unknown method "' . $action->getController() . '::' . $action->getMethod() . '"';
		parent::__construct($message, $code);
	}
}




class app_Controller extends bab_Controller implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    protected $app = null;



    /**
     * @var string[]
     */
    protected $reloadSelectors = array();



    /**
     * @param string $reloadSelector
     */
    public function addReloadSelector($reloadSelector)
    {
        $this->reloadSelectors[$reloadSelector] = $reloadSelector;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getReloadSelectors()
    {
        return $this->reloadSelectors;
    }


    /**
     * Can be used in a controller to check if the current request was made by ajax.
     * @var bool
     */
    protected $isAjaxRequest = null;


    /**
     * Name of method to use to create action
     * @var string
     */
    public $createActionMethod = 'createActionForTg';



    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app = null)
    {
        $this->setApp($app);
    }
    
    /**
     *
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     */
    public function __call($name, $arguments = array())
    {
        switch (true) {
            case ($component = $this->App()->getComponentByName(ucfirst($name))) != false:
                return call_user_func_array(array($component, 'controller'), $arguments);
            default:
                throw new \app_Exception(sprintf($this->App()->translate('Unknown action %s'), $name));
        }
    }


    /**
     * {@inheritDoc}
     * @see app_Object::setApp()
     */
    public function setApp(Func_App $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see app_Object::App()
     */
    public function App()
    {
        return $this->app;
    }


    /**
     * @return string
     */
    protected function getControllerTg()
    {
        return $this->App()->controllerTg;
    }


    /**
     * Can be used in a controller to check if the current request was made by ajax.
     * @return bool
     */
    public function isAjaxRequest()
    {
        if (!isset($this->isAjaxRequest)) {
            $this->isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
        }
        return $this->isAjaxRequest;
    }


    /**
     * Ensures that the user is logged in.
     *
     * Tries to use the appropriate authentication type depending on the situation.
     */
    public function requireCredential($message = null)
    {
        if ($this->isAjaxRequest()) {
            $authType = 'Basic';
        } else {
            $authType = '';
        }

        if (!isset($message)) {
            $message = $this->App()->translate('You must be logged in to access this page.');
        }

        bab_requireCredential($message, $authType);
    }




    /**
     * Returns an instance of the specific controller class.
     * If $proxy is true, a proxy to this controller is returned instead.
     *
     * @deprecated use $App->ControllerProxy() instead
     *
     * @param string	$classname
     * @param bool		$proxy
     * @return app_Controller
     */
    public static function getInstance($classname, $proxy = true)
    {


        // If the app object was not specified (through the setApp() method)
        // we try to select one according to the classname prefix.
        list($prefix) = explode('_', __CLASS__);
        $functionalityName = ucwords($prefix);
        $App = @bab_functionality::get('App/' . $functionalityName);

        if (!$App)
        {
            throw new app_Exception('Faild to autodetect functionality App/' . $functionalityName.', the getInstance method is deprecated, use $App->ControllerProxy() instead');
        }

        $controller = $App->ControllerProxy($classname, $proxy);

        return $controller;
    }


    /**
     * Dynamically creates a proxy class for this controller with all public, non-final and non-static functions
     * overriden so that they return an action (Widget_Action) corresponding to each of them.
     *
     * @param string $classname
     * @return app_Controller
     */
    static function getProxyInstance(Func_App $App, $classname)
    {
        $class = new ReflectionClass($classname);
        $proxyClassname = $class->getShortName() . self::PROXY_CLASS_SUFFIX;
        if (!class_exists($proxyClassname)) {
            $classStr = 'class ' . $proxyClassname . ' extends ' . $class->name . ' {' . "\n";
            $methods = $class->getMethods();

            $classStr .= '	public function __construct(Func_App $App) {
                $this->setApp($App);
            }' . "\n";

            foreach ($methods as $method) {
                if ($method->name === '__construct' || !$method->isPublic() || $method->isStatic() || $method->isFinal() || $method->name === 'setApp' || $method->name === 'App') {
                    continue;
                }


                $classStr .= '	public function ' . $method->name . '(';
                $parameters = $method->getParameters();
                $parametersStr = array();
                foreach ($parameters as $parameter) {

                    if ($parameter->isDefaultValueAvailable()) {
                        $parametersStr[] = '$' . $parameter->name . ' = ' . var_export($parameter->getDefaultValue(), true);
                    } else {
                        $parametersStr[] = '$' . $parameter->name;
                    }
                }
                $classStr .= implode(', ', $parametersStr);
                $classStr .= ') {' . "\n";
                $classStr .= '		$args = func_get_args();' . "\n";
                $classStr .= '		return $this->getMethodAction(__FUNCTION__, $args);' . "\n";
                $classStr .= '	}' . "\n";
            }
            $classStr .= '}' . "\n";

            // We define the proxy class
            eval($classStr);
        }

        $proxy = new $proxyClassname($App);

        //$proxy = bab_getInstance($proxyClassname);
        return $proxy;
    }


    /**
     * @return self
     */
    public function proxy()
    {
        return self::getProxyInstance($this->App(), get_class($this));
    }

    /**
     * Get proxy class with action from the new addon controller
     * @return self
     */
    protected function quickProxy()
    {
        $proxy = $this->proxy();
        $proxy->createActionMethod = 'createActionForAddon';
        return $proxy;
    }



    /**
     * {@inheritDoc}
     * @see bab_Controller::getObjectName()
     */
    protected function getObjectName($classname)
    {
        list($objectname) = explode('Controller', $classname);
        if($this->App()->getComponentByName($objectname)){
            return strtolower($objectname);
        }
        list(, $objectname) = explode('_Ctrl', $classname);
        return strtolower($objectname);
    }

    /**
     * Returns the action object corresponding to the current object method $methodName
     * with the parameters $args.
     *
     * @param string $methodName
     * @param array $args
     * @return Widget_Action	Or null on error.
     */
    protected function getMethodAction($methodName, $args)
    {
        $fullClassName = substr(get_class($this), 0, -strlen(self::PROXY_CLASS_SUFFIX));
        $className = join('', array_slice(explode('\\', $fullClassName), -1));

        $reflector = new ReflectionClass(get_class($this));
        $parent = $reflector->getParentClass();
        if($parent){
            $parentMethod = $parent->getMethod('__construct');
            $docComment = $parentMethod->getDocComment();
            if (strpos($docComment, '@isComponentController') !== false) {
                $fullClassName = $parent->getName();
            }
        }

        $rc = new ReflectionClass($fullClassName);

        if (!$rc->hasMethod($methodName)) {
            throw new bab_InvalidActionException($fullClassName . '::' . $methodName);
        }
        $method = new ReflectionMethod($fullClassName, $methodName);

        $objectName = $this->getObjectName($className);

        $parameters = $method->getParameters();
        $actionParams = array();
        $argNumber = 0;
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            if (isset($args[$argNumber])) {
                $actionParams[$parameterName] = $args[$argNumber];
            } elseif ($parameter->isDefaultValueAvailable()) {
                $actionParams[$parameterName] = $parameter->getDefaultValue();
            } else {
                $actionParams[$parameterName] = null;
            }
            $argNumber++;
        }

        $action = new Widget_Action();

        $action->setMethod($this->getControllerTg(), $objectName . '.' . $methodName, $actionParams);

        $docComment = $method->getDocComment();
        if (strpos($docComment, '@ajax') !== false) {
            $action->setAjax(true);
        }
        if (strpos($docComment, '@requireSaveMethod') !== false && method_exists($action, 'setRequireSaveMethod')) {
            $action->setRequireSaveMethod(true);
        }
        if (strpos($docComment, '@requireDeleteMethod') !== false && method_exists($action, 'setRequireDeleteMethod')) {
            $action->setRequireDeleteMethod(true);
        }

        return $action;
    }


    /**
     * @return Widget_Action
     */
    protected function createActionForTg($idx, $actionParams)
    {
        $action = new Widget_Action();

        $action->setMethod($this->App()->controllerTg, $idx, $actionParams);

        return $action;
    }


    /**
     * @return Widget_Action
     */
    protected function createActionForAddon($idx, $actionParams)
    {
        $App = $this->App();
        $action = new Widget_Action();

        $action->setParameters($actionParams);


        list(,,$file) = explode('/', $App->controllerTg);

        $action->setParameter('addon', $App->getAddonName().'.'.$file);
        $action->setParameter('idx', $idx);

        return $action;
    }



    /**
     * Tries to dispatch the action to the correct sub-controller.
     *
     * @param Widget_Action 	$action
     * @return mixed
     */
    final public function execute(Widget_Action $action)
    {
        $this->app->setCurrentComponent(null);

        $method = $action->getMethod();

        if (!isset($method) || '' === $method) {
            return false;
        }

        list($objectName, ) = explode('.', $method);

        if (!method_exists($this, $objectName)) {
            /* @var $component app_Component */
            $component = $this->app->getComponentByName(ucfirst($objectName));
            if($component){
                $this->app->setCurrentComponent($component);
                $objectController = $component->controller(false);
            }
            else{
                header('HTTP/1.0 400 Bad Request');
                throw new app_UnknownActionException($action);
            }
        }

        if(!isset($component)){
            $objectController = $this->{$objectName}(false);
            if ( ! ($objectController instanceof app_Controller)) {
                return false;
            }
        }

        try {
            $returnedValue = $objectController->execAction($action);
        } catch (app_AccessException $e) {

            if (!bab_isUserLogged() && $e->requireCredential) {
                bab_requireCredential($e->getMessage());
            } else {
                if ($this->isAjaxRequest()) {

                    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);

                    die(
                        bab_json_encode(
                            array(
                                'exception' => 'app_AccessException',
                                'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                                'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                            )
                        )
                    );
                }
                app_addPageInfo($e->getMessage());

                $returnedValue = $this->App()->Ui()->Page();
                // $returnedValue->addError($e->getMessage());
            }

        } catch (app_SaveException $e) {

            if ($this->isAjaxRequest()) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');

                die(
                    bab_json_encode(
                        array(
                            'exception' => 'app_SaveException',
                            'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                            'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                        )
                    )
                );
            }

        } catch (app_DeletedRecordException $e) {
            $this->deletedItemPage($action, $e);

        } catch (app_NotFoundException $e) {
            $this->notFoundPage($action, $e);

        } catch (ORM_Exception $e) {
            $this->errorPage($e);
        }

        $W = bab_Widgets();

        if ($returnedValue instanceof Widget_Displayable_Interface) {

            if ($returnedValue instanceof Widget_BabPage && !$this->isAjaxRequest()) {

                // If the action returned a page, we display it.
                $returnedValue->displayHtml();

            } else {

                $htmlCanvas = $W->HtmlCanvas();
                if (self::$acceptJson) {
                    $itemId = $returnedValue->getId();
                    $returnArray = array($itemId => $returnedValue->display($htmlCanvas));
                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: application/json');
                    die(bab_json_encode($returnArray));
                } else {
                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: text/html');
                    if ($returnedValue instanceof Widget_Page && method_exists($returnedValue, 'getPageTitle')) {
                        $pageTitle = $returnedValue->getPageTitle();

                        if (method_exists($htmlCanvas, 'sendPageTitle')) {
                            $htmlCanvas->sendPageTitle($pageTitle);
                        } else {
                            header('X-Cto-PageTitle: ' . $pageTitle);
                        }
                    }
                    $html = $returnedValue->display($htmlCanvas);
                    die($html);
                }
            }

        } elseif (is_array($returnedValue)) {

            $htmlCanvas = $W->HtmlCanvas();
            $returnedArray = array();
            foreach ($returnedValue as $key => &$item) {
                if ($item instanceof Widget_Displayable_Interface) {
                    $returnedArray[$item->getId()] = $item->display($htmlCanvas);
                } else {
                    $returnedArray[$key] = $item;
                }

            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            die(bab_convertStringFromDatabase(bab_json_encode($returnedArray), bab_charset::UTF_8));

        } elseif (true === $returnedValue || is_string($returnedValue)) {

            if ($this->isAjaxRequest()) {
                $body = bab_getBody();
                $json = array();
                $json['messages'] = array();
                foreach ($body->messages as $message) {
                    $json['messages'][] = array(
                        'level' => 'info',
                        'content' => $message,
                        'time' => 4000
                    );
                }
                foreach ($body->errors as $message) {
                    $json['messages'][] = array(
                        'level' => 'danger',
                        'content' => $message
                    );
                }
                if ($objectController->getReloadSelectors()) {
                    $json['reloadSelector'] = implode(',', $objectController->getReloadSelectors());
                }
                echo bab_json_encode($json);
                die;
            }

        }

        return $returnedValue;
    }

    private function deletedItemPage(Widget_Action $action, app_DeletedRecordException $e)
    {
        if ($this->isAjaxRequest()) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');

            die(
                app_json_encode(

                    array(
                        'exception' => 'app_SaveException',
                        'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                        'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                    )
                    )
                );

        }
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);

        $App = $this->App();
        $W = bab_Widgets();

        $page = $App->Ui()->Page();

        $dialog = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'))
        ->addClass(Func_Icons::ICON_LEFT_16);

        $page->addItem($dialog);

        //TRANSLATORS: %s can be task, contact, organization ...
        $dialog->addItem($W->Title(sprintf(app_translate('This %s has been deleted'), $e->getObjectTitle()), 1));

        if ($e instanceof app_DeletedRecordException) {
            $dialog->addItem(
                $W->VBoxItems(
                    $e->getDeletedBy(),
                    $e->getDeletedOn()
                    )
                );
        }

        $page->displayHtml();

    }




    private function notFoundPage(Widget_Action $action, app_NotFoundException $e)
    {
        if ($this->isAjaxRequest()) {
            $message = sprintf(app_translate('This %s does not exists'), $e->getObjectTitle() . ' (' . $e->getId() . ')');
            $json = array(
                'messages' => array(
                    array(
                        'level' => 'danger',
                        'content' => $message
                    )
                )
            );
            echo bab_json_encode($json);
            die;
        }
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

        $App = $this->App();
        $W = bab_Widgets();

        $page = $App->Ui()->Page();
        $page->addClass(Func_Icons::ICON_LEFT_16);

        //TRANSLATORS: %s can be task, contact, organization ...
        $page->addItem($W->Title(sprintf(app_translate('This %s does not exists'), $e->getObjectTitle()), 1));
        $page->displayHtml();

    }



    private function errorPage(Exception $e)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal server error', true, 500);

        require_once $GLOBALS['babInstallPath'] . 'utilit/uiutil.php';
        $popup = new babBodyPopup();
        $popup->babEcho(
            '<h1>' . $this->App()->translate('There was a problem when trying to perform this operation') . '</h1>'
            . '<h2 class="error">' . $this->App()->translate('Additional debugging information:') . '</h2>'
            . '<p class="error">' . $e->getMessage() . ' ' .  $e->getFile() . ' ' . $e->getLine()  . ' ' . $e->getTraceAsString() . '</p>'
            );


        die($popup->printout());
    }



    /**
     * Method to call before saving
     * @since 1.0.22
     * @return self
     */
    public function requireSaveMethod()
    {
        if ('GET' === $_SERVER['REQUEST_METHOD']) {
            throw new app_Exception('Method not allowed');
        }

        return $this;
    }


    /**
     * Method to call before deleting
     * @since 1.0.22
     * @return self
     */
    public function requireDeleteMethod()
    {
        if ('GET' === $_SERVER['REQUEST_METHOD']) {
            throw new app_Exception('Method not allowed');
        }

        return $this;
    }


    /**
     * Custom fields
     * @return app_CtrlCustomField
     */
    public function CustomField($proxy = true)
    {
        require_once APP_CTRL_PATH . 'customfield.ctrl.php';
        return $this->App()->ControllerProxy('app_CtrlCustomField', $proxy);
    }


    /**
     * Custom sections
     * @return app_CtrlCustomSection
     */
    public function CustomSection($proxy = true)
    {
        require_once APP_CTRL_PATH . 'customsection.ctrl.php';
        return $this->App()->ControllerProxy('app_CtrlCustomSection', $proxy);
    }
}
