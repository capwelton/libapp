<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/controller.class.php';



abstract class app_CtrlRecord extends app_Controller
{
    /**
     * This methods returns the Record classname managed by this controller.
     * The default method guess the record classname based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return string
     */
    protected function getRecordClassName()
    {
        $App = $this->App();
        if($currentComponent = $App->getCurrentComponent()){
            return $currentComponent->getRecordClassName();
        }
        list(, $recordClassname) = explode('_Ctrl', $this->getClass());
        return $recordClassname;
    }


    /**
     * This methods returns the Record set managed by this controller.
     * The default method guess the record set based on the controller name.
     * It can be overriden by inherited controllers.
     *
     * @return app_RecordSet
     */
    protected function getRecordSet()
    {
        $App = $this->App();
        if($currentComponent = $App->getCurrentComponent()){
            return $currentComponent->recordSet();
        }
        $recordClassname = $this->getRecordClassName();
        $recordSetClassname = $recordClassname . 'Set';

        $recordSet = $App->$recordSetClassname();
        return $recordSet;
    }


    /**
     * @return app_RecordSet
     */
    protected function getEditRecordSet()
    {
        return $this->getRecordSet();
    }


    /**
     * This methods returns the Record set used to save records
     *
     * @return app_RecordSet
     */
    protected function getSaveRecordSet()
    {
        return $this->getRecordSet();
    }

    /**
     * This methods returns the Record set used to delete records
     *
     * @return app_RecordSet
     */
    protected function getDeleteRecordSet()
    {
        return $this->getSaveRecordSet();
    }


    /**
     *
     * @param Widget_Action|Widget_Link|Widget_Action[]|Widget_Link[]|array $actions
     * @param Widget_Item $box
     * @return string
     */
    protected function createMenu($actions, $icon = true, $showLabel = false)
    {
        $canvas = bab_Widgets()->HtmlCanvas();
        $html = '';
        if ($actions instanceof Widget_Action) {
            $text = '';
            if ($showLabel) {
                $text = $actions->getTitle();
            }
            $html = '<li><a class="icon ' . $actions->getIcon() .  '" href="' . $actions->url() . '">' . $text . '</a></li>';
        } elseif ($actions instanceof Widget_Link) {
            $html = '<li>' . $actions->display($canvas) . '</li>';
        } elseif (is_array($actions)) {
            if (isset($actions['items'])) {
                $items = $actions['items'];
            } else {
                $items = $actions;
            }
            foreach ($items as $action) {
                $html .= $this->createMenu($action, true, true);
            }
            if (isset($actions['icon'])) {

                $html = '<li class="dropdown">'
                    . '<a href="#" class="' . $actions['icon'] . ' icon dropdown-toogle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'
                    . ($actions['icon'] === 'actions-context-menu'/*Func_Icons::ACTIONS_CONTEXT_MENU*/ ? '' : '<span class="caret"></span>')
                    . '</a>'
                    . '<ul class="dropdown-menu dropdown-menu-right ' . Func_Icons::ICON_LEFT_SYMBOLIC . '">'
                    . $html
                    . '</ul>';
            }
        }

        return $html;
    }


    /**
     *
     * @return string[]
     */
    protected function getAvailableModelViewTypes()
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $types = array();

        $recordClassname = $this->getRecordClassName();

        $viewClassname =  $recordClassname . 'TableView';
        if (method_exists($Ui, $viewClassname)) {
            $types['table'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::ACTIONS_VIEW_LIST_TEXT,
                'label' => $App->translate('Detailed list')
            );
        }

        $viewClassname =  $recordClassname . 'CardsView';
        if (method_exists($Ui, $viewClassname)) {
            $types['cards'] = array(
                'classname' => $viewClassname,
                'icon' => 'actions-view-list-cards', //Func_Icons::ACTIONS_VIEW_LIST_CARDS
                'label' => $App->translate('Cards')
            );
        }

        $viewClassname =  $recordClassname . 'MapView';
        if (method_exists($Ui, $viewClassname)) {
            $types['map'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::APPS_PREFERENCES_SITE,
                'label' => $App->translate('Map')
            );
        }

        $viewClassname =  $recordClassname . 'CalendarView';
        if (method_exists($Ui, $viewClassname)) {
            $types['calendar'] = array(
                'classname' => $viewClassname,
                'icon' => Func_Icons::APPS_CALENDAR,
                'label' => $App->translate('Calendar')
            );
        }

        return $types;
    }



    /**
     * Returns an array of field names and descriptions that can be used in Full/CardFrames.
     *
     * @return string[]
     */
    public function getAvailableDisplayFields()
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();

        $availableFields = array();

        foreach ($recordSet->getFields() as $name => $field) {
            $fieldName = $field->getName();
            $fieldDescription = $field->getDescription();
            if (substr($fieldName, 0, 1) !== '_') {
                $fieldDescription = $App->translate($fieldDescription);
            }
            if (empty($fieldDescription)) {
                $fieldDescription = $fieldName;
            }
            $availableFields[$name] = array(
                'name' => $fieldName,
                'description' => $fieldDescription
            );
        }

        $availableFields['_spacer'] = array(
            'name' => '_spacer',
            'description' => '[Spacer]'
        );

        return $availableFields;
    }



    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param int $customSectionId      The id of the section used to create the editor
     * @param string $itemId
     *
     * @throws app_AccessException
     * @return app_Page
     */
    public function editSection($id, $customSectionId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $App->Ui()->Page();

        $customSectionSet = $App->CustomSectionSet();
        $customSection = $customSectionSet->get(
            $customSectionSet->id->is($customSectionId)
        );

        $page->setTitle($customSection->name);

        $recordSet = $this->getEditRecordSet();

        $recordClassname = $this->getRecordClassName();
        $editorClassname =  $recordClassname . 'SectionEditor';
        /* @var $editor app_RecordEditor */
        $editor = $Ui->$editorClassname();
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor->setId($itemId);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));

        $editor->recordSet = $recordSet;
        $section = $editor->sectionContent($customSectionId);
        $section->setSizePolicy('widget-60em');

        $editor->addItem($section);

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $editor->setRecord($record);
        } else {
            if (!$recordSet->isCreatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
            $editor->setRecord($record);
        }

        $editor->isAjax = bab_isAjaxRequest();

        $page->addItem($editor);

        return $page;
    }


    /**
     * @param string $itemId
     * @return string
     */
    public function getModelViewDefaultId($itemId = null)
    {
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_modelView';
        }

        return $itemId;
    }

    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return app_TableModelView
     */
    protected function modelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        if($currentComponent = $App->getCurrentComponent()){
            $Ui = $currentComponent->ui();
        }
        else{
            $Ui = $App->Ui();
        }

        $recordSet = $this->getRecordSet();

        $recordClassname = $this->getRecordClassName();

        $itemId = $this->getModelViewDefaultId($itemId);

        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }

        //$types = $this->getAvailableModelViewTypes();

        switch ($type) {
            case 'cards':
                $tableviewClassname =  $currentComponent ? 'cardsView' : $recordClassname . 'CardsView';
                break;

            case 'map':
                $tableviewClassname =  $currentComponent ? 'mapView' : $recordClassname . 'MapView';
                break;

            case 'calendar':
                $tableviewClassname =  $currentComponent ? 'calendarView' : $recordClassname . 'CalendarView';
                break;

            case 'table':
            default:
                $tableviewClassname =  $currentComponent ? 'tableView' : $recordClassname . 'TableView';
                break;
        }


        /* @var $tableview widget_TableModelView */
        $tableview = $Ui->$tableviewClassname();

        $tableview->setRecordController($this);

        $tableview->setId($itemId);

        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        $filter = $tableview->getFilterValues();

        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }

        $conditions = $tableview->getFilterCriteria($filter);

        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $records = $recordSet->select($conditions);

//         print_r($records->getSelectQuery());

        $tableview->setDataSource($records);



        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();

            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }

        $tableview->allowColumnSelection();

        return $tableview;
    }




    /**
     * @return app_Editor
     */
    protected function recordEditor($itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        if($currentComponent = $App->getCurrentComponent()){
            $editor = $currentComponent->ui()->editor();
        }
        else{

            $recordClassname = $this->getRecordClassName();
            $editorClassname =  $recordClassname . 'Editor';
            $editor = $Ui->$editorClassname();
        }

        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor->setId($itemId);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');

        $editor->isAjax = bab_isAjaxRequest();

        return $editor;
    }



    /**
     * @param widget_TableModelView $tableView
     * @return app_Toolbar
     */
    protected function toolbar($tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $proxy = $this->proxy();

        $filter = $tableView->getFilterValues();

        $toolbar = $W->FlowItems();
        $toolbar->addClass('widget-toolbar', Func_Icons::ICON_LEFT_16);

        $viewTypes = $this->getAvailableModelViewTypes();

        if (count($viewTypes) > 1) {
            $viewsBox = $W->Items();
            $viewsBox->setSizePolicy('pull-right');
//            $viewsBox->addClass('btn-group', Func_Icons::ICON_LEFT_16);
            $toolbar->addItem($viewsBox);

            $filteredViewType = $this->getFilteredViewType($tableView->getId());

            foreach ($viewTypes as $viewTypeId => $viewType) {
                $viewsBox->addItem(
                    $W->Link(
                        '',
                        $proxy->setFilteredViewType($tableView->getId(), $viewTypeId)
                    )->addClass('icon', $viewType['icon'] . ($filteredViewType=== $viewTypeId ? ' active' : ''))
//                    ->setSizePolicy('btn btn-xs btn-default ' . ($filteredViewType === $viewTypeId ? 'active' : ''))
                    ->setTitle($viewType['label'])
                    ->setAjaxAction()
                );
            }
        }

        if (method_exists($proxy, 'exportSelect')) {
            $toolbar->addItem(
                $W->Link(
                    $App->translate('Export'),
                    $proxy->exportSelect($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
                ->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        if (method_exists($proxy, 'printList')) {
            $toolbar->addItem(
                $W->Link(
                    $App->translate('Print'),
                    $proxy->printList($filter)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
                ->setOpenMode(Widget_Link::OPEN_POPUP)
            );
        }

        return $toolbar;
    }




    /**
     * @param string $itemId
     * @return boolean
     */
    public function toggleFilterVisibility($itemId)
    {
        $W = bab_Widgets();

        $filterVisibility = $this->getFilterVisibility($itemId);
        $filterVisibility = !$filterVisibility;
        $W->setUserConfiguration($itemId . '/filterVisibility', $filterVisibility);

        return true;
    }


    /**
     * @param string $itemId
     * @return boolean
     */
    protected function getFilterVisibility($itemId)
    {
        $W = bab_Widgets();
        $filterVisibility = $W->getUserConfiguration($itemId . '/filterVisibility');
        if (!isset($filterVisibility)) {
            $filterVisibility = false;
            $W->setUserConfiguration($itemId . '/filterVisibility', $filterVisibility);
        }
        return $filterVisibility;
    }


    /**
     * Returns the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @return string
     */
    protected function getFilteredViewType($itemId)
    {
        $W = bab_Widgets();
        $type = $W->getUserConfiguration($itemId . '/viewType');
        if (!isset($type)) {
            $type = 'table';
        }
        return $type;
    }


    /**
     * Sets the current filtered view type (table, cards...)
     * @param string        $itemId     The model view widget id
     * @param string        $type       'table, 'cards'...
     */
    public function setFilteredViewType($itemId, $type = null)
    {
        $W = bab_Widgets();
        $W->setUserConfiguration($itemId . '/viewType', $type);

        return true;
    }


    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();

        $filter = $view->getFilterValues();

        $filterPanel = $view->advancedFilterPanel($filter);

        if ($this->getFilterVisibility($itemId)) {
            $filterPanel->addClass('show-filter');
        } else {
            $filterPanel->addClass('hide-filter');
        }
        $toolbar = $this->toolbar($view);


        if ($view->isColumnSelectionAllowed()) {
            $columnSelectionMenu = $view->columnSelectionMenu($view->getVisibleColumns());
            $toolbar->addItem($columnSelectionMenu);
        }

        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->filteredView(null, null, $view->getId()));

        return $box;
    }



    /**
     * @param array $filter
     * @param string $type
     * @return array
     */
    protected function getListItemMenus($filter = null, $type = null)
    {
        $itemMenus = array();

//         $tabs = $this->getTabs();

//         if (count($tabs) > 0) {
//             $itemMenus['all'] = array (
//                 'label' => $App->translate('All'),
//                 'action' => $this->proxy()->setCurrentTab('all')
//             );
//         }
//         foreach ($tabs as $tab) {
//             $itemMenus[$tab] = array(
//                 'label' => $tab,
//                 'action' => $this->proxy()->setCurrentTab($tab)
//             );
//         }

        return $itemMenus;
    }



    /**
     * @param array	$filter
     * @param string $type
     * @return Widget_Page
     */
    public function displayList($filter = null, $type = null)
    {
        $W = bab_Widgets();

        $page = $W->BabPage();
        $page->addClass('app-page-list');

        $itemMenus = $this->getListItemMenus();

        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }

        $filteredView = $this->filteredView($filter, $type);

        $page->addItem($filteredView);

        return $page;
    }


    /**
     *
     * @param array $filter
     * @return Widget_Page
     */
    public function exportSelect($filter = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate('Export list'));

        $tableview = $this->modelView($filter, 'table');

        $editor = $Ui->ExportSelectEditor(str_replace('::', '__', __METHOD__), $tableview, $filter);

        $editor->setSaveAction(
            $this->proxy()->exportList(),
            $App->translate('Export')
        );
        $page->addItem($editor);

        return $page;
    }





    /**
     * @param	array	$filter
     */
    public function exportList($filter = null, $format = 'csv', $columns = null, $inline = true, $filename = 'export')
    {
        $App = $this->App();

        $page = $App->Ui()->Page();
        $page->addClass('app-page-list');

        $tableview = $this->modelView($filter, 'table', $columns);
        $tableview->allowColumnSelection(false);

        switch ($format) {
            case 'xlsx':
                $tableview->downloadXlsx($filename . '.xlsx');
                break;
            case 'xls':
                $tableview->downloadExcel($filename . '.xls');
                break;
            case 'ssv':
                $tableview->downloadCsv($filename . '.csv', ';', $inline, 'Windows-1252');
                break;
            case 'csv':
            default:
                $tableview->downloadCsv($filename . '.csv', ',', $inline, bab_charset::getIso());
                break;
        }
    }


    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return app_Page
     */
    public function display($id, $view = '')
    {
        $App = $this->App();
        $page = $App->Ui()->Page();

        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isReadable()) {
            throw new app_AccessException($App->translate('You do not have access to this page.'));
        }

        $W = bab_Widgets();
        $Ui = $App->Ui();

        $recordClassname = $this->getRecordClassName();
        $fullFrameClassname =  $recordClassname . 'FullFrame';
        $fullFrame = $Ui->$fullFrameClassname($record, $view);


        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $App->Controller()->Search(false)->history($record->getRef(), $maxHistory, 0, true)
        );
        $fullFrame->addItem(
            $W->Section(
                $App->translate('History'),
                $historyFrame,
                2
            )->setFoldable(true)
            ->addClass('compact')
        );

        $page->addItem($fullFrame);


        // Actions
        $page->addContextItem($this->getDisplayActionFrame($page, $record));

        return $page;
    }



    protected function getDisplayActionFrame(Widget_Page $page, app_Record $record)
    {
        $actionsFrame = $page->ActionsFrame();
        return $actionsFrame;
    }



    /**
     * Returns a page containing an editor for the record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function edit($id = null, $view = '')
    {
        $W = bab_Widgets();
        $App = $this->App();

        $recordSet = $this->getEditRecordSet();

        $page = $W->BabPage();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate($recordSet->getDescription()));

        $editor = $this->recordEditor();

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
        } else {
            if (!$recordSet->isCreatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
        }
        $editor->setRecord($record);

        $page->addItem($editor);

        return $page;
    }

    /**
     * Returns a page containing an editor for the tags record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function editTags($id = null, $view = '')
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getEditRecordSet();

        $page = $W->BabPage();
        $page->addClass('app-page-editor');

        $page->setTitle($App->translate($recordSet->getDescription()));

        $editor = $Ui->TagsEditor();

        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $editor->setRecord($record);
        } else {
            if (!$recordSet->isCreatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            $record = $recordSet->newRecord();
            $editor->setRecord($record);
        }

        $page->addItem($editor);

        return $page;
    }


    public function ok()
    {
        return true;
    }



    /**
     *
     * @param  $data
     */
    public function create($data)
    {

    }


    /**
     *
     * @param  $data
     */
    public function update($data)
    {

    }



    /**
     *
     */
    public function read($id)
    {

    }


    /**
     * Get the message to display on delete next page
     * @return string | true
     */
    protected function getDeletedMessage(ORM_Record $record)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();

        $recordTitle = $record->getRecordTitle();

        $message = sprintf($App->translate('%s has been deleted'), $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"');
        return $message;
    }

    /**
     * Get the message to display on create next page
     * @return string | true
     */
    protected function getCreatedMessage()
    {
        return true;
    }

    /**
     * Get the message to display on modification next page
     * @param app_Record $record The record before the modification
     * @return string | true
     */
    protected function getModifedMessage(ORM_Record $record)
    {
        return true;
    }


    /**
     * Method to check reord before saving
     * @param app_Record $record
     */
    protected function preSave(ORM_Record $record, $data)
    {

    }


    /**
     * Method for post save actions on record
     * @param app_Record $record
     */
    protected function postSave(ORM_Record $record, $data)
    {

    }


    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        $recordSet = $this->getSaveRecordSet();
        $pk = $recordSet->getPrimaryKey();
        $message = null;

        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);

            if (!$record->isUpdatable()) {
                throw new app_AccessException('Access denied');
            }

            $message = $this->getModifedMessage($record);
        } else {

            $record = $recordSet->newRecord();

            if (!$recordSet->isCreatable()) {
                throw new app_AccessException('Access denied');
            }

            $message = $this->getCreatedMessage();
        }

        if (!isset($record)) {
            throw new app_SaveException($App->translate('The record does not exists'));
        }


        if (!isset($data)) {
            throw new app_SaveException($App->translate('Nothing to save'));
        }

        $record->setFormInputValues($data);


        $this->preSave($record, $data);
        if ($record->save()) {
            $this->postSave($record, $data);
            if (is_string($message)) {
                $this->addMessage($message);
            }

            $this->addReloadSelector('.depends-' . $this->getRecordClassName());
            return true;
        }

        return true;
    }


    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function delete($id)
    {
        $this->requireDeleteMethod();
        $recordSet = $this->getDeleteRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isDeletable()) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }
        $deletedMessage = $this->getDeletedMessage($record);

        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }

        $this->addReloadSelector('.depends-' . $record->getClassName());

        return true;
    }


    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);

        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Deletion'));

        if (!isset($record)) {
            $page->addItem(
                $W->Label($App->translate('This element does not exists.'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }


        $form = new app_Editor($App);

        $form->addItem($W->Hidden()->setName('id'));

        $recordTitle = $record->getRecordTitle();

        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $form->addItem($W->Title($App->translate('Confirm delete?'), 6));

        $form->addItem($W->Html(bab_toHtml($App->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Delete'))
         );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }









    /**
     * Deletes the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function remove($id)
    {
        $this->requireDeleteMethod();
        $recordSet = $this->getDeleteRecordSet();

        $record = $recordSet->request($id);

        if (!$record->isRemovable()) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $App = $this->App();
        $deletedMessage = $App->translate('The element has been moved to the trash');

        if ($recordSet->delete($recordSet->id->is($record->id), app_TraceableRecord::DELETED_STATUS_IN_TRASH)) {
            if (is_string($deletedMessage)) {
                $this->addMessage($deletedMessage);
            }
        }

        $this->addReloadSelector('.depends-' . $record->getClassName());

        return true;
    }


    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmRemove($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);

        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Move to trash'));

        $form = new app_Editor($App);

        $recordTitle = $record->getRecordTitle();

        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $form->addItem($W->Title($App->translate('Confirm delete?'), 6));

//         $form->addItem($W->Html(bab_toHtml($App->translate('It will not be possible to undo this deletion.'))));

        $confirmedAction = $this->proxy()->remove($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($App->translate('Move to trash'))
            );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }




    /**
     * Restores the specified record.
     *
     * @requireDeleteMethod
     *
     * @param string $id        The record id
     * @return boolean
     */
    public function restore($id)
    {
        $this->requireSaveMethod();
        $recordSet = $this->getRecordSet();

        $records = $recordSet->select($recordSet->id->is($id), true);

        $record = null;
        foreach ($records as $record) {
        }
        if (! isset($record)) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }
        if (!$record->isRestorable()) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $App = $this->App();
        $deletedMessage = $App->translate('The element has been restored from the trash');

        $record->deleted = app_TraceableRecord::DELETED_STATUS_EXISTING;
        $record->save();
        $this->addMessage($deletedMessage);

        $this->addReloadSelector('.depends-' . $record->getClassName());

        return true;
    }

    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmRestore($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->is($id), true);

        $record = null;
        foreach ($records as $record) {
        }
        if (! isset($record)) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }

        $page = $Ui->Page();

        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Restore from trash'));

        $form = new app_Editor($App);

        $recordTitle = $record->getRecordTitle();

        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));

        $confirmedAction = $this->proxy()->restore($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Restore from trash'))
        );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);

        return $page;
    }

    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredTrash($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();

        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();

        if (isset($filter)) {
            $view->setFilterValues($filter);
        }
        $filter = $view->getFilterValues();

        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $view->displaySubTotalRow(true);
        }


        $recordSet = $this->getRecordSet();
        $view->setRecordSet($recordSet);
        $view->addDefaultColumns($recordSet);

        $conditions = $view->getFilterCriteria($filter);
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );

        $conditions = $conditions->_AND_(
            $recordSet->deleted->is(app_TraceableRecord::DELETED_STATUS_IN_TRASH)
        );

        $records = $recordSet->select($conditions, true);

        $view->setDataSource($records);

        $view->allowColumnSelection();

        $filterPanel = $view->advancedFilterPanel($filter);

//         $toolbar = $this->toolbar($view);

        $box = $W->VBoxItems(
//             $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->filteredTrash(null, null, $view->getId()));

        return $box;
    }



    public function displayTrash()
    {
        $App = $this->App();
        $Ui = $App->Ui();

        $page = $Ui->Page();

//         $page->setTitle($App->translate('Trash'));


        $trashView = $this->filteredTrash();

        $page->addItem($trashView);

        $itemMenus = $this->getListItemMenus();
        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
        $page->setCurrentItemMenu('displayTrash');

        return $page;
    }

    protected function getClass()
    {
        return get_class($this);
    }


        /**
     * @return string[]
     */
    public function getFilterNames()
    {
        $id = $this->getModelViewDefaultId();

        $filters = array();

        $registry = bab_getRegistryInstance();

        $userId = '0';
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/filters');

        while ($filterName = $registry->fetchChildKey()) {
            $filters[] = $filterName;
        }

        $userId = bab_getUserId();
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $id. '/filters');

        while ($filterName = $registry->fetchChildKey()) {
            $filters[] = $filterName;
        }

        return $filters;
    }



    /**
     * Sets the currently used filter for the page.
     *
     * @param string $filterName
     * @return boolean
     */
    public function setCurrentFilterName($filterName)
    {
        $W = bab_Widgets();
        $tableview = $this->modelView(null, 'table');
        $W->setUserConfiguration($tableview->getId(). '/currentFilterName', $filterName, 'widgets', false);

        $registry = bab_getRegistryInstance();
        $userId = '0';
        $registry->changeDirectory('/widgets/user/' . $userId . '/' . $tableview->getId() . '/filters');
        $filterValues = $registry->getValue($filterName);

        if (!$filterValues) {
            $filterValues = $W->getUserConfiguration($tableview->getId() . '/filters/' . $filterName, 'widgets', false);
        }

        $tableview = $this->modelView($filterValues, 'table');
        $tableview->setFilterValues($filterValues['values']);

        return true;
    }



    /**
     *
     * @return string|null
     */
    protected function getCurrentFilterName()
    {
        $W = bab_Widgets();
        $id = $this->getModelViewDefaultId();
        $filter = $W->getUserConfiguration($id. '/currentFilterName', 'widgets', false);

        return $filter;
    }



    /**
     * @return boolean
     */
    public function resetFilters($name = null)
    {
        if (isset($name)) {
            $W = bab_Widgets();
            $filter = $W->getUserConfiguration($tableview->getId(). '/filters/' . $name, 'widgets', false);
            $filterValues = $filter['values'];
        } else {
            $filterValues = null;
        }

        $tableview = $this->modelView($filterValues, 'table');
        $tableview->setFilterValues($filterValues);

        return true;
    }



    /**
     * Saves the values of the current view filter under the specified name.
     *
     * @param string $name
     * @return boolean
     */
    public function saveCurrentFilter($name = null, $description = null)
    {
        $W = bab_Widgets();
        $tableview = $this->modelView(null, 'table');
        $filterValues = $tableview->getFilterValues();

        $data = array(
            'description' => $description,
            'values' => $filterValues,
        );

        $W->setUserConfiguration($tableview->getId(). '/filters/' . $name, $data, 'widgets', false);

        return true;
    }


    /**
     * Display a dialog to propose saving the current view filter under a specific name.
     *
     * @return app_Page
     */
    public function confirmSaveCurrentFilter()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $currentFilterName = $this->getCurrentFilterName();
        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $currentFilterName, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($App->translate('Save current filter'));

        $form = new app_Editor($App);


        $form->addItem(
            $W->Html(bab_toHtml($App->translate('Save the current filter values so that you can reuse them later.')))
        );

        $form->addItem(
            $W->LabelledWidget(
                $App->translate('Name'),
                $W->LineEdit()->setValue($currentFilterName),
                'name'
            )
        );
        $form->addItem(
            $W->LabelledWidget(
                $App->translate('Description'),
                $W->TextEdit()->setValue($filter['description']),
                'description'
            )
        );

        $confirmedAction = $this->proxy()->saveCurrentFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Save'))
        );

        $page->addItem($form);

        return $page;
    }


    /**
     * Display a dialog to propose saving the current view filter under a specific name.
     *
     * @return app_Page
     */
    public function editFilter($name)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($App->translate('Edit filter'));

        $form = new app_Editor($App);

        $form->addItem(
            $W->LabelledWidget(
                $App->translate('Name'),
                $W->LineEdit()->setValue($name),
                'name'
            )
        );
        $form->addItem(
            $W->LabelledWidget(
                $App->translate('Description'),
                $W->TextEdit()->setValue($filter['description']),
                'description'
            )
        );

        $confirmedAction = $this->proxy()->saveCurrentFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Save'))
        );

        $page->addItem($form);

        return $page;
    }


    public function manageFilters()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $page = $Ui->Page();

        $page->setTitle($App->translate('Manage filters'));

        $filtersBox = $W->VBoxItems();

        $page->addItem($filtersBox);

        $filterNames = $this->getFilterNames();
        foreach ($filterNames as $filterName) {
            $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $filterName, 'widgets', false);

            $filterBox = $W->FlowItems()
                ->setVerticalAlign('top')
                ->setSizePolicy('widget-list-element');
            $filterBox->addItem(
                $W->VBoxItems(
                    $W->Label($filterName)
                        ->addClass('widget-strong'),
                    $W->Label($filter['description'])
                )->setSizePolicy('widget-90pc')
            );
            $filterBox->addItem(
                $W->FlowItems(
                    $W->Link('', $this->proxy()->editFilter($filterName))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link('', $this->proxy()->confirmDeleteFilter($filterName))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                )->setSizePolicy('widget-10pc')
                ->addClass(Func_Icons::ICON_LEFT_16)
            );
            $filtersBox->addItem($filterBox);
        }

        $page->setReloadAction($this->proxy()->manageFilters());

        return $page;
    }



    /**
     * Display a dialog to propose deleting a view filter.
     *
     * @param string $name
     * @return app_Page
     */
    public function confirmDeleteFilter($name)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $filter = $W->getUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($App->translate('Delete filter'));

        $form = new app_Editor($App);

        $form->addItem(
            $W->Html(bab_toHtml($filter['description']))
        );

        $form->setHiddenValue('name', $name);

        $confirmedAction = $this->proxy()->deleteFilter();
        $form->addButton(
            $W->SubmitButton()
                ->setAjaxAction($confirmedAction)
                ->setLabel($App->translate('Delete'))
        );

        $page->addItem($form);

        return $page;
    }


    /**
     *
     * @param string $name
     */
    public function deleteFilter($name = null)
    {
        $this->requireDeleteMethod();

        $W = bab_Widgets();
        $W->deleteUserConfiguration($this->getModelViewDefaultId() . '/filters/' . $name, 'widgets', false);

        return true;
    }
}
