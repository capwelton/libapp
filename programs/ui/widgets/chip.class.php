<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');

/**
 * A app_Chip
 */
class app_Chip extends Widget_Widget
{
    protected $App;
    protected $label;
    protected $labelIcon;
    protected $action;
    protected $actionIcon;
    
    public function __construct(Func_App $App, $label, $labelIcon = null, $action = null, $actionIcon = null, $id = null)
    {
        parent::__construct($id);
        
        $this->App = $App;
        $this->label = $label;
        $this->labelIcon = $labelIcon;
        $this->action = $action;
        $this->actionIcon = $actionIcon;
    }
    
    public function App()
    {
        return $this->App();
    }

    public function setApp(Func_App $App)
    {
        $this->App = $App;
        return $this;
    }
    
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'app-chip';
        return $classes;
    }
    
    public function chip()
    {
        $W = bab_Widgets();
        
        $html = "<div class='chip'>";
        
        if(isset($this->labelIcon)){
            $icon = $W->Label('')->setIconFormat(16, 'left')->setIcon($this->labelIcon);
            $html .= $icon->display($W->HtmlCanvas());
        }
        
        $html .= $this->label;
        
        if(isset($this->action)){
            $icon = $W->Link('', $this->action)->setIconFormat(16, 'left')->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);
            $icon->setIcon(isset($this->actionIcon) ? $this->actionIcon : Func_Icons::ACTIONS_DIALOG_CANCEL);
            $icon->addClass('chip-right-action');
            $html .= $icon->display($W->HtmlCanvas());
        }
        
        $html .= "</div>";
            
        return $html;
    }
    
    public function display(Widget_Canvas $canvas)
    {
        $output = parent::display($canvas);
        $appAddon = bab_getAddonInfosInstance('libapp');
        $output .= $canvas->loadAddonStyleSheet($appAddon, 'widgets/chip.css');
        
        $output .= $canvas->html($this->getId(), $this->getClasses(), $this->chip());
        
        return $output;
    }
}
