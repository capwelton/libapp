<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of customfield of articles from back office
 *
 */
class app_CustomFieldTableView extends app_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$App = $record->App();

		$editAction = $App->Controller()->CustomField()->edit($record->id);

		switch ($fieldPath) {

            case '_actions_':
                $box = $W->FlowItems();
                if ($record->isUpdatable()) {
                    $box->addItem(
                        $W->Link('', $App->Controller()->CustomField()->edit($record->id))
                            ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link('', $App->Controller()->CustomField()->confirmDelete($record->id))
                            ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;

			case 'mandatory':
			case 'visible_in_shop':
				if (self::getRecordFieldValue($record, $fieldPath))
				{
					return $W->Label($App->translate('Yes'));
				} else {
					return $W->Label($App->translate('No'));
				}
				break;
		}

		return parent::computeCellContent($record, $fieldPath);
	}



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        /* @var $recordSet app_CustomFieldSet */
        $this->addColumn(
            app_TableModelViewColumn($recordSet->name)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->description)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->object)
        );
//         $this->addColumn(
//             app_TableModelViewColumn($recordSet->section)
//         );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->fieldtype)
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->mandatory, $App->translate('Mandatory'))
                ->addClass('widget-10em')
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->importable)
                ->addClass('widget-10em')
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->searchable, $App->translate('Searchable'))
                ->addClass('widget-10em')
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->visible, $App->translate('Visible in list'))
                ->addClass('widget-10em')
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-nowrap', Func_Icons::ICON_LEFT_SYMBOLIC)
        );
        if ($App->onlineShop) {
            $this->addColumn(widget_TableModelViewColumn($recordSet->visible_in_shop, $App->translate('Visible in online shop')));
        }

        $this->setDefaultSortField('name:up');
    }
}







/**
 *
 */
class app_CustomFieldEditor extends app_Editor
{
    /**
     *
     * @var app_CustomField
     */
    protected $customfield = null;


    public function __construct(Func_App $App, app_CustomField $customfield = null, $id = null, Widget_Layout $layout = null)
    {
        $this->customfield = $customfield;

        parent::__construct($App, $id, $layout);
        $this->setName('customfield');
        $this->colon();

        $this->addFields();
        $this->addButtons();

        $this->setHiddenValue('tg', $App->controllerTg);

        if (isset($customfield)) {
            $this->setHiddenValue('customfield[id]', $customfield->id);
            $values = $customfield->getValues();

            if (!empty($this->customfield->enumvalues)) {
                $values['enumvalues'] = unserialize($this->customfield->enumvalues);
            } else {
                $values['enumvalues'] = array('0' => '');
            }

            $this->setValues($values, array('customfield'));
        }
    }


    protected function addFields()
    {
        $App = $this->App();

        $this->addItem($this->object());
        $this->addItem($this->name());
        $this->addItem($this->section());
        $this->addItem($this->description());
        $this->addItem($this->fieldtype());
        $this->addItem($this->mandatory());
        $this->addItem($this->searchable());
        $this->addItem($this->visible());
        $this->addItem($this->importable());
        if ($App->onlineShop) {
            $this->addItem($this->visible_in_shop());
        }
    }


    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;

        $this->addButton(
            $W->SubmitButton()
                ->setLabel($App->translate('Save'))
                ->validate(true)
                ->setAction($App->Controller()->CustomField()->save())
                ->setAjaxAction()
        );

        $this->addButton(
            $W->SubmitButton()
                ->addClass('widget-close-dialog')
                ->setLabel($App->translate('Cancel'))
        );
	}


    protected function section()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Section'),
            $W->LineEdit()
                ->addClass('widget-100pc')
                ->setMaxSize(255),
            'section'
        );
    }

    protected function importable()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Importable'),
            $W->Checkbox(),
            'importable'
        );
    }

    protected function searchable()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Searchable'),
            $W->Checkbox(),
            'searchable'
        );
    }

    protected function visible()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Visible in list'),
            $W->Checkbox(),
            'visible'
        );
    }

    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Name'),
            $W->LineEdit()
                ->addClass('widget-100pc')
                ->setMandatory(true, $App->translate('The name is mandatory')),
            'name'
        );
    }

    protected function description()
    {
        $App = $this->App();
        $W = $this->widgets;

        return $this->labelledField(
            $App->translate('Description'),
            $W->TextEdit()
                ->setLines(2)
                ->addClass('widget-100pc'),
            'description'
        );
    }

    protected function fieldtype()
    {
        $App = $this->App();
        $W = $this->widgets;

        $select = $W->Select();
        $select->setOptions($App->CustomFieldSet()
            ->getFieldTypes());

        $fieldTypeItem = $this->labelledField(
            $App->translate('Field type'),
            $select,
            'fieldtype'
        );

        $fieldValuesItem = $W->MultiField();
        $fieldValuesItem->setName('enumvalues');
        $fieldValuesItem->addItem($W->Label($App->translate('List of available values')));
        $values = array();

        if (isset($this->customfield) && !empty($this->customfield->enumvalues)) {
            $values = unserialize($this->customfield->enumvalues);

            foreach ($values as $name => $text) {
                $fieldValuesItem->addItem(
                    $W->LineEdit()->setName((string) $name)
                );
            }
        }

        if (empty($values)) {
            $fieldValuesItem->addItem(
                $W->LineEdit()->setName('1')
            );
        }

        $select->setAssociatedDisplayable($fieldValuesItem, array('Enum', 'Set'));

        return $W->FlowItems($fieldTypeItem, $fieldValuesItem)
            ->setHorizontalSpacing(2, 'em')
            ->setVerticalAlign('top');
    }


    protected function object()
    {
        $App = $this->App();
        $W = $this->widgets;

//         if (isset($this->customfield)) {
//             $set = $this->customfield->getParentSet();

//             $layout = $W->FlowLayout()
//             ->setSpacing(.4, 'em');
//             $layout->addItem(
//                 $W->Label($App->translate('This field will be used in'))
//                 ->colon()
//             );
//             $layout->addItem(
//                 $W->Label($set->output($this->customfield->object()))
//             );

//             return $layout;
//         }

        return $this->labelledField(
            $App->translate('This field will be used in'),
            $W->Select()
            ->setOptions($App->CustomFieldSet()
                ->getObjects()),
            'object'
        );
    }



	public function mandatory()
	{
		$App = $this->App();
		$W = $this->widgets;

		return $this->labelledField(
			$App->translate('Mandatory field'),
			$W->Checkbox(),
			__FUNCTION__
		);
	}

	public function visible_in_shop()
	{
		$App = $this->App();
		$W = $this->widgets;

		return $this->labelledField(
			$App->translate('Visible in online shop'),
			$W->Checkbox(),
			__FUNCTION__
		);
	}
}
