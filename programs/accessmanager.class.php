<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * The app_AccessManager class
 *
 * @method Func_App App()
 */
class app_AccessManager implements app_Object_Interface
{
    /**
     * @var Func_App
     */
    private $app = null;


    /**
     * @param Func_App $app
     */
    public function __construct(Func_App $app)
    {
        $this->setApp($app);
    }

    /**
     * Forces the Func_App object to which this object is 'linked'.
     *
     * @param Func_App	$app
     * @return self
     */
    public function setApp(Func_App $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Get APP object to use with this SET
     *
     * @return Func_App
     */
    public function App()
    {
        return $this->app;
    }


    /**
     *
     * @param app_RecordSet $recordSet
     * @param string        $accessType
     * @param int           $user
     */
    public function getAccessCriterion(app_RecordSet $recordSet, $accessType, $user = null)
    {
        return $recordSet->none();
    }


    /**
     * Check if at least one access criterion is not a FalseCriterion.
     *
     * If all access criterion are FalseCritertion, return true, otherwise return false.
     *
     * @param app_RecordSet $recordSet
     * @param string        $accessType
     * @param int|null      $user
     *
     * @return boolean
     */
    public function hasNoAccess(app_RecordSet $recordSet, $accessType, $user = null)
    {
        $criterion = $this->getAccessCriterion($recordSet, $accessType, $user);
        if (! $criterion instanceof ORM_FalseCriterion) {
            return false;
        }
        return true;
    }


    /**
     *
     * @param app_Record    $record
     * @param string        $accessType
     * @param int           $user
     */
    public function getAccess(app_Record $record, $accessType, $user = null)
    {
        $recordSet = $record->getParentSet();
        $criterion = $this->getAccessCriterion($recordSet, $accessType, $user);

        $matches = $recordSet->select($criterion->_AND_($recordSet->id->is($record->id)));

        return ($matches->count() !== 0);
    }
}
