<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008-2018 by CANTICO ({@link http://www.cantico.fr})
 * @copyright Copyright (c) 2019 by CapWelton ({@link https://www.capwelton.com})
 */



/**
 * @property ORM_StringField    $object
 * @property ORM_StringField    $action
 * @property ORM_StringField    $criterion
 * @property app_RoleSet        $role
 * @method app_RoleSet          role()
 *
 * @method app_RoleAccess   get(mixed $criteria)
 * @method app_RoleAccess   request(mixed $criteria)
 * @method app_RoleAccess[] select(\ORM_Criteria $criteria = null)
 * @method app_RoleAccess   newRecord()
 */
class app_RoleAccessSet extends app_RecordSet
{
    /**
     *
     * @param Func_App $App
     */
    public function __construct(Func_App $App)
    {
        parent::__construct($App);

        $App = $this->App();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('object')
                ->setDescription('Object'),
            ORM_StringField('action')
                ->setDescription('Action'),
            ORM_StringField('criterion')
                ->setDescription('Criterion')
        );

        $this->hasOne('role', $App->RoleSetClassName());
    }
}



/**
 * @property string     $object
 * @property string     $action
 * @property string     $criterion
 * @property app_Role   $role
 * @method app_Role     role()
 *
 * @method app_RoleAccessSet getParentSet()
 */
class app_RoleAccess extends app_Record
{
}

