<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


$App = app_App();
$App->includeTraceableRecordSet();

/**
 * @property ORM_StringField $name
 * @property ORM_StringField $fieldname
 * @property ORM_TextField $description
 * @property ORM_EnumField $object
 * @property ORM_EnumField $fieldtype
 * @property ORM_TextField $enumvalues
 * @property ORM_BoolField $mandatory
 *
 * @method app_CustomField      get(mixed $criteria)
 * @method app_CustomField      request(mixed $criteria)
 * @method app_CustomField[]    select(\ORM_Criteria $criteria = null)
 * @method app_CustomField      newRecord()
 *
 * @method Func_App App()
 */
class app_CustomFieldSet extends app_TraceableRecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setDescription('Custom field');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('name')
            ->setDescription('Field label'),
            ORM_StringField('fieldname')
            ->setDescription('Name of field used in table, not modifiable'),
            ORM_TextField('description')
            ->setDescription('Description'),
            ORM_EnumField('object', $this->getObjects())
            ->setDescription('Applies to'),
            ORM_EnumField('fieldtype', $this->getFieldTypes())
            ->setDescription('Field type'),
            ORM_TextField('enumvalues')
            ->setDescription('serialized array of values for enum fields'),
            ORM_BoolField('mandatory')
            ->setDescription('Mandatory'),
            
            ORM_StringField('section')
            ->setDescription($App->translate('Section')),
            ORM_BoolField('importable')
            ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
            ->setDescription($App->translate('Importable')),
            ORM_BoolField('searchable')
            ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
            ->setDescription($App->translate('Searchable')),
            ORM_BoolField('visible')
            ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
            ->setDescription($App->translate('Column in list'))
            );
        
        if ($App->onlineShop) {
            $this->addFields(
                ORM_BoolField('visible_in_shop')
            );
        }
    }
    
    
    
    public function save(ORM_Record $record, $noTrace = false)
    {
        if (!$record->fieldname) {
            $record->fieldname = $this->getFieldName($record->name);
        }
        
        return parent::save($record, $noTrace);
    }
    
    
    /**
     * Get a field name from a custom field name
     * @param string $name
     * @throws app_SaveException
     * @return string
     */
    public function getFieldName($name)
    {
        // create the field name automatically
        $name = bab_removeDiacritics($name);
        $name = preg_replace('/[^a-zA-Z0-9]+/', '', $name);
        $name = mb_strtolower($name);
        
        if (empty($name)) {
            throw new app_SaveException($this->App()->translate('The name is mandatory'));
        }
        
        return '_' . $name;
    }
    
    
    
    /**
     * List of objects where custom fields are applicable.
     *
     * @return string[]
     */
    public function getObjects()
    {
        $App = $this->App();
        
        $arr = array();
        
        if (isset($App->Article)) {
            $arr['Article'] = $App->translate('Products database');
        }
        
        return $arr;
    }
    
    /**
     * list of ORM fields to use for a custom field
     */
    public function getFieldTypes()
    {
        $App = $this->App();
        
        return array(
            'String'    => $App->translate('Line edit'),
            'Text'      => $App->translate('Text edit'),
            'Bool'      => $App->translate('Checkbox'),
            'Int'       => $App->translate('Integer number'),
            'Decimal'   => $App->translate('Decimal number'),
            'Date'      => $App->translate('Date'),
            'DateTime'  => $App->translate('Date and time'),
            'Time'      => $App->translate('Time'),
            'Enum'      => $App->translate('Selection list'),
            'Set'       => $App->translate('Multi-selection list'),
            'Url'       => $App->translate('Url'),
            'Email'     => $App->translate('Email address'),
            'File'      => $App->translate('File'),
        );
    }
    
    
    
    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }
    
    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @property string $name
 * @property string $fieldname
 * @property string $description
 * @property string $object
 * @property string $fieldtype
 * @property string $enumvalues
 * @property bool   $mandatory
 *
 * @method Func_App App()
 */
class app_CustomField extends app_TraceableRecord
{
    
    /**
     * @return ORM_Field
     */
    public function getORMField()
    {
        switch ($this->fieldtype) {
            case 'Enum':
                $field = ORM_EnumField($this->fieldname, $this->getEnumValues());
                break;
                
            case 'Decimal':
                $field = ORM_DecimalField($this->fieldname, 2);
                break;
                
            case 'Bool':
                $field = ORM_BoolField($this->fieldname);
                $field->setOutputOptions(
                    $this->App()->translate('No'),
                    $this->App()->translate('Yes')
                );
                break;
                
            default:
                $function = 'ORM_'.$this->fieldtype.'Field';
                $field = $function($this->fieldname);
                break;
        }
        
        $field->setDescription($this->description);
        
        return $field;
    }
    
    
    /**
     * @return string[]
     */
    public function getEnumValues()
    {
        return unserialize($this->enumvalues);
    }
    
    
    /**
     * @return Widget_LabelledWidget
     */
    public function getWidget()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        switch ($this->fieldtype) {
            case 'Text':
                $widget = $W->TextEdit()->setLines(3)->setColumns(70);
                break;
                
            case 'Bool':
                $widget = $W->Checkbox();
                break;
                
            case 'Enum':
                $options = array('' => '') + $this->getEnumValues();
                $widget = $W->Select()->setOptions($options);
                break;
                
            case 'Date':
                $widget = $W->DatePicker();
                break;
                
            case 'DateTime':
                $widget = $W->DateTimePicker();
                break;
                
            case 'Int':
                $widget = $W->RegExpLineEdit();
                $widget->setRegExp(Widget_RegExpLineEdit::INT_FORMAT)
                ->setSubmitMessage(sprintf($App->translate('The field %s should be a integer number'), $this->name))
                ->setSize(10);
                break;
                
            case 'Decimal':
                $widget = $W->RegExpLineEdit();
                $widget->setRegExp(Widget_RegExpLineEdit::FLOAT_FORMAT)
                ->setSubmitMessage(sprintf($App->translate('The field %s should be a decimal number'), $this->name))
                ->setSize(10);
                break;
                
            case 'Url':
                $widget = $W->UrlLineEdit();
                break;
                
            case 'Email':
                $widget = $W->EmailLineEdit();
                break;
                
            default:
            case 'String':
                $widget = $W->LineEdit()->setSize(70)->setMaxSize(255);
                break;
        }
        
        $labelledItem = $W->LabelledWidget($this->name, $widget, null);
        $labelledItem->setName($this->fieldname);
        
        if (!empty($this->description)) {
            $labelledItem->setDescription($this->description);
        }
        
        return $labelledItem;
    }
}
