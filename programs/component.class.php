<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

require_once dirname(__FILE__). '/record.ctrl.php';

class app_Component 
{
    const ENTRY = 'Entry';
    const COUNTRY = 'Country';
    const ADDRESS = 'Address';
    
    private $app;
    
    private $set;
    private $controller;
    private $ui;
    
    private $controllerObject;
    private $uiObject;
    
    public function __construct(Func_App $app, $set = null, $controller = null, $ui = null)
    {
        $this->app = $app;
        $this->set = $set;
        $this->controller = $controller;
        $this->ui = $ui;
    }
    
    /**
     * Returns a multidimensional array with information about required and optional components.
     * The returned array may have 0, 1 or 2 keys : 'requiredComponents' and 'optionalComponents'.
     * Each of these keys is associated to an array of strings for each component name.
     * @return array
     */
    public function checkDependencies()
    {
        $main = new ReflectionClass($this->set);
        $dependencies = array();
        if($main->hasMethod('getRequiredComponents')){
            $requiredComponents = $this->recordSet()->getRequiredComponents();
            foreach ($requiredComponents as $requiredComponent){
                $dependencies['requiredComponents'][] = $requiredComponent;
            }
        }
        if($main->hasMethod('getOptionalComponents')){
            $optionalComponents = $this->recordSet()->getOptionalComponents();
            foreach ($optionalComponents as $optionalComponent){
                $dependencies['optionalComponents'][] = $optionalComponent;
            }
        }
        
        return $dependencies;
    }
    
    /**
     * If the component record set has a method onUpdate, this method is called.
     * This method should only be called in the synchronizeComponentsSql method of the addon.
     */
    public function onUpdate()
    {
        $main = new ReflectionClass($this->set);
        if($main->hasMethod('onUpdate')){
            $this->recordSet()->onUpdate();
        }
    }
    
    /**
     * @return app_ComponentCtrlRecord
     */
    public function controller($proxy = true)
    {
        if(!isset($this->controllerObject)){
            $this->app->includeRecordController();
            $ctrl = $this->controller;
            $this->controllerObject = new $ctrl($this->app, $this);
        }
        if($proxy){
            return $this->controllerObject->proxy();
        }
        return $this->controllerObject;
    }
    
    /**
     * @return app_ComponentCtrlRecord
     */
    public function proxy()
    {
        return $this->controller();
    }
    
    /**
     * @return app_ComponentUi
     */
    public function ui()
    {
        if(!isset($this->uiObject)){
            $ui = $this->ui;
            $this->uiObject = new $ui($this->app);
        }
        return $this->uiObject;
    }
    
    public function recordSet()
    {
        $set = $this->set;
        return new $set($this->app);
    }
    
    public function getSetClassName()
    {
        $reflectionClass = new ReflectionClass($this->set);
        return $this->app->classPrefix . $reflectionClass->getShortName();
    }
    
    public function getRecordClassName()
    {
        return substr($this->getSetClassName(), 0, -3);
    }
}

interface app_ComponentUi
{
    public function tableView($id = null);
    
    public function editor($id = null, \Widget_Layout $layout = null);
}

class app_ComponentCtrlRecord extends app_CtrlRecord
{
    private $component;
    
    /**
     * @param Func_App $app
     * @isComponentController
     */
    public function __construct(Func_App $app = null, app_Component $component = null)
    {
        $this->setApp($app);
        $this->setComponent($component);
    }
    
    public function setComponent(app_Component $component = null)
    {
        $this->component = $component;
        return $this;
    }
        
    public function getComponent()
    {
        return $this->component;
    }
    
    protected function getRecordSet()
    {
        return $this->getComponent()->recordSet();
    }
    
    protected function getRecordClassName()
    {
        return $this->getComponent()->getRecordClassName();
    }
}