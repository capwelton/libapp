��    t      �  �   \      �	     �	     �	     �	     
  !   
     6
  
   G
     R
     [
     b
     h
     q
     w
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     	          !     -     ;     A     S     _     s     |     �     �     �     �     �     �     �     �     �                         !     2     O     g     ~     �  .   �     �  	   �     �     �  	   �             %        ?     ^     z     �     �     �     �     �     �     �     �     �     �                2     7     K  @   b     �     �     �     �     �     �     �  	      '   
  ,   2  '   _  '   �     �     �  Z   �  <   ;  .   x  9   �     �     �          0     8     @     H     M     ^     r     v     �     �  $   �  *   �     �     �  �  �     �     �      �       *        F  
   Y     d     k     s     z     �     �     �     �     �     �     �  	   �            	   (     2     D     U     a     m  	     !   �     �     �  
   �     �     �     �               &  .   <     k     �     �  	   �     �     �  
   �     �  !   �     �               +  4   9     n     w     �     �     �     �     �  %   �     �  #        >     P  "   g     �     �  6   �     �     �     �     �       *   "     M     e     q     �  ^   �       
   "     -     5     O     i     {     �  ,   �  +   �  ,     *   1     \     s  T   �  Q   �  5   :  A   p     �     �     �                          %     6     R     V     h     v  %   z  7   �     �     �         2       "           C          	       O   f   
   l      a      =   A   *   '   p   <       %          F      P          3           N   H               Q   (       c   [         #   5   :   9   b      _   S   B   4       Y                      E   ;   ]           i       I   n          ^                  M           r   )         +       T           t      e   0       K   `   h            j   \   $   6      R       G      W       J   U   m   @   8   !          1   ,   >   /                    g   .   7      D   V   s      d   L       &             k   -   o       ?   Z       X   q    %s has been deleted Access denied Add a custom field Add a section Additional debugging information: Available fields CSV (.csv) Calendar Cancel Cards Checkbox Class Confirm delete? Content charset Create new field Date Date and time Decimal number Delete Delete filter Delete view layout Deleted Deleted by: %s Deleted on: %s Deletion Description Detailed list Draft Edit custom field Edit filter Edit view layout %s Editable Email address Existing Export Export list Export view layout Exported columns Failed to write file to disk. Fields layout File Filter Foldable Folded Format History Horizontal label Horizontal label (very wide) Horizontal label (wide) Import sections layout In trash Integer number It will not be possible to undo this deletion. Label Line edit Manage filters Manage filters... Mandatory Map Microsoft CSV (.csv) Microsoft Excel 2007-2013 XML (.xlsx) Microsoft Excel 97-2003 (.xls) Missing a temporary folder. More criteria Move to trash Multi-selection list Name No No file was uploaded. No label Nothing to save Print Products database Reset Reset filter to default values Restore from trash Save Save current filter Save current filter... Save the current filter values so that you can reuse them later. Saved filters Search Section Section visibility Select all / none Selection list Size policy Text edit The element has been moved to the trash The element has been restored from the trash The field %s should be a decimal number The field %s should be a integer number The name is mandatory The record does not exists The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. The uploaded file exceeds the upload_max_filesize directive. The uploaded file was only partially uploaded. There was a problem when trying to perform this operation This %s does not exists This %s has been deleted This element does not exists. Title 1 Title 2 Title 3 Type UTF-8 (with BOM) Unknown File Error. Url Vertical label View layout name Yes You do not have access to this page. You must be logged in to access this page. _euro_ object Project-Id-Version: 
PO-Revision-Date: 2020-02-17 15:44+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ../programs
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: app_translate;app_translate:1,2;translate:1,2;translate
X-Poedit-SearchPath-0: .
 Suppression de %s effectuée Accès refusé Ajouter un champ supplémentaire Ajouter une section Information de débogage supplémentaire : Champs disponibles CSV (.csv) Agenda Annuler Cartes Case à cocher Classe Confirmer la suppression ? Charset du contenu Créer un nouveau champ Date Date et heure Nombre décimal Supprimer Supprimer le filtre Supprimer la vue Supprimé Supprimé par: %s Supprimé le: %s Suppression Description Liste détaillée Brouillon Modifier le champ supplémentaire Modifier le filtre Modifier la vue %s Modifiable Adresse de messagerie Existant Exporter Exporter la liste Exporter la vue Exporter les colonnes Impossible d'écrire le fichier sur le disque. Disposition des champs Fichier Filtre Repliable Repliée Format Historique Libellé horizontal Libellé horizontal (très large) Libellé horizontal (large) Importer une vue Dans la corbeille Nombre entier Il ne sera pas possible d'annuler cette suppression. Libellé Ligne de texte Gérer les filtres Gérer les filtres... Obligatoire Plan Microsoft CSV (.csv) Microsoft Excel 2007-2013 XML (.xlsx) Microsoft Excel 97-2003 (.xls) Le fichier temporaire est manquant. Plus de critères Mettre à la corbeille Liste déroulante multi-sélection Nom Non Aucun fichier n'a été téléchargé vers le serveur. Pas de libellé Rien à enregistrer Imprimer Base de donnés des produits Réinitialiser Remettre les valeurs du filtre par défaut Retirer de la corbeille Enregistrer Enregistrer le filtre actuel Enregistrer le filtre actuel... Enregistre les valeurs du filtre actuel de manière à pouvoir le réutiliser ultérieurement. Filtres enregistrés Rechercher Section Visibilité de la section Sélectionner tout / rien Liste déroulante Gestion de la taille Texte sur plusieurs lignes L'élément a été placé dans la corbeille L'élément a été retiré de la corbeille Le champ %s devrait être un nombre décimal Le champ %s devrait être un nombre entier Le nom est obligatoire Cet enregistrement n'existe pas Le fichier à télécharger excède la taille maximale autorisée par le formulaire. Le fichier à télécharger excède la taille maximale autorisée par le serveur. Le fichier n'a été que partiellement téléchargé. Un problème est survenu en essayant d'effectuer cette opération Cet objet %s n'existe pas Cet objet %s à été supprimé Cet élément n'existe pas. Titre 1 Titre 2 Titre 2 Type UTF-8 (avec BOM) Erreur de fichier inconnue. Url Libellé vertical Nom de la vue Oui Vous n'avez pas accès à cette page. Vous devez être connecté pour accéder à cette page. € objet 